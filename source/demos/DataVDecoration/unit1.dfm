object Form1: TForm1
  Left = 0
  Top = 0
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 1017
  ClientWidth = 360
  Color = 4328985
  TransparentColor = True
  TransparentColorValue = 4328985
  Font.Charset = ANSI_CHARSET
  Font.Color = 8627522
  Font.Height = -17
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 23
  object Label1: TLabel
    AlignWithMargins = True
    Left = 20
    Top = 10
    Width = 320
    Height = 50
    HelpKeyword = 'dvdecoration'
    HelpContext = 1
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitWidth = 639
  end
  object Label2: TLabel
    AlignWithMargins = True
    Left = 20
    Top = 130
    Width = 320
    Height = 50
    HelpKeyword = 'dvdecoration'
    HelpContext = 3
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitTop = 220
    ExplicitWidth = 639
  end
  object Label4: TLabel
    AlignWithMargins = True
    Left = 20
    Top = 310
    Width = 320
    Height = 50
    HelpKeyword = 'dvdecoration'
    HelpContext = 6
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitTop = 580
    ExplicitWidth = 639
  end
  object Label5: TLabel
    AlignWithMargins = True
    Left = 20
    Top = 70
    Width = 320
    Height = 50
    HelpKeyword = 'dvdecoration'
    HelpContext = 2
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitWidth = 639
  end
  object Label6: TLabel
    AlignWithMargins = True
    Left = 20
    Top = 190
    Width = 320
    Height = 50
    HelpKeyword = 'dvdecoration'
    HelpContext = 4
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitWidth = 639
  end
  object Label8: TLabel
    AlignWithMargins = True
    Left = 20
    Top = 430
    Width = 320
    Height = 50
    HelpKeyword = 'dvdecoration'
    HelpContext = 8
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitTop = 520
    ExplicitWidth = 639
  end
  object Label9: TLabel
    AlignWithMargins = True
    Left = 20
    Top = 250
    Width = 320
    Height = 50
    HelpKeyword = 'dvdecoration'
    HelpContext = 5
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitWidth = 639
  end
  object Label10: TLabel
    AlignWithMargins = True
    Left = 20
    Top = 370
    Width = 320
    Height = 50
    HelpKeyword = 'dvdecoration'
    HelpContext = 7
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitTop = 820
    ExplicitWidth = 639
  end
  object Label12: TLabel
    AlignWithMargins = True
    Left = 100
    Top = 490
    Width = 160
    Height = 160
    HelpKeyword = 'dvdecoration'
    HelpContext = 9
    Margins.Left = 100
    Margins.Top = 10
    Margins.Right = 100
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitLeft = 20
    ExplicitWidth = 320
  end
  object Label13: TLabel
    AlignWithMargins = True
    Left = 20
    Top = 720
    Width = 320
    Height = 50
    Hint = '{"dwattr":"title='#39'deweb'#39'"}'
    HelpKeyword = 'dvdecoration'
    HelpContext = 11
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitTop = 1240
    ExplicitWidth = 639
  end
  object Label14: TLabel
    AlignWithMargins = True
    Left = 20
    Top = 660
    Width = 320
    Height = 50
    HelpKeyword = 'dvdecoration'
    HelpContext = 10
    Margins.Left = 20
    Margins.Top = 10
    Margins.Right = 20
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitTop = 1090
    ExplicitWidth = 639
  end
  object Label16: TLabel
    AlignWithMargins = True
    Left = 80
    Top = 780
    Width = 200
    Height = 200
    HelpKeyword = 'dvdecoration'
    HelpContext = 12
    Margins.Left = 80
    Margins.Top = 10
    Margins.Right = 80
    Margins.Bottom = 0
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 8627522
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitTop = 670
  end
end
