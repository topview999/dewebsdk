﻿unit unit_login;

interface

uses
     //
     dwBase,

     //
     SynCommons,

     //
     Forms,
     Winapi.Windows, Winapi.Messages, System.SysUtils,
  Data.Win.ADODB, Vcl.StdCtrls, Vcl.Controls, Vcl.ExtCtrls, System.Classes, Vcl.Imaging.pngimage,
  Data.DB;

type
  TForm_Login = class(TForm)
    Panel_Login: TPanel;
    Image_Logo: TImage;
    ADOQuery1: TADOQuery;
    Edit_User: TEdit;
    Edit_Password: TEdit;
    Panel5: TPanel;
    Image_Captcha: TImage;
    Panel2: TPanel;
    Edit_Captcha: TEdit;
    Panel1: TPanel;
    CheckBox1: TCheckBox;
    StaticText1: TStaticText;
    Button1: TButton;
    Panel3: TPanel;
    Image_qq: TImage;
    Image_wx: TImage;
    Image_wb: TImage;
    Image_bg: TImage;
    Panel_LoginForm: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
    Form_Login   : TForm_Login;
const
    _config = '{'
            +'"tablename":"dw_user",'                   //数据表名
            +'"fields":["uid","username","salt","psd"],'//关键字段名，依次分别为用户id,用户名，盐，密码
            +'"successurl":"/dwms",'                    //成功后跳转的URL
            +'"cookie":["bbs_user","bbs123"],'          //成功后写入的COOKIE名称,和加密字符串
            +'"error":"账号或密码或验证码错误"'
            +'}';

implementation

{$R *.dfm}


procedure TForm_Login.Button1Click(Sender: TObject);
var
    joConfig    : Variant;
    joUser      : Variant;
    //
    iUid        : Integer;
    sName       : String;
    sPassword   : String;
    sSalt       : String;
    sCookie     : string;
    sValue      : string;
    sInfo       : string;
    bLogin      : Boolean;
begin
    //检查配置文件是否合法的JSON字符串.如果不是,则提示,退出
    if not dwStrIsJson(_config) then begin
        dwMessage('配置信息不正确,请检查!','error',self);
        Exit;
    end;
    if lowercase(Edit_Captcha.Text) <> lowercase(dwGetProp(Image_Captcha,'captcha')) then begin
        dwMessage('验证码不正确,请检查!','error',self);
        Exit;
    end;

    //创建配置JSON对象,以备后面使用
    joConfig    := _JSON(_config);

    //检查用户名信息是否正确
    try
        //设置登录标识默认为未登录
        bLogin  := False;


        //在数据表中内查询
        ADOQuery1.Close;
        ADOQuery1.SQL.Text   := 'SELECT ['+joConfig.fields._(0)+'],['+joConfig.fields._(1)+'],['+joConfig.fields._(2)+'],['+joConfig.fields._(3)+'] '
                +' FROM '+joConfig.tablename
                +' WHERE '+joConfig.fields._(1)+'='''+Trim(Edit_User.Text)+'''';
        ADOQuery1.Open;

        //检查ovnr
        if not ADOQuery1.IsEmpty then begin
            //得到数据表内的信息备用
            iUid        := ADOQuery1.Fields[0].AsInteger;
            sName       := ADOQuery1.Fields[1].AsString;
            sSalt       := ADOQuery1.Fields[2].AsString;
            sPassword   := ADOQuery1.Fields[3].AsString;
            //
            //检查正确性
            if sPassword = dwGetMD5(dwGetMD5(Edit_Password.Text)+sSalt) then begin
                //设置登录正确
                bLogin  := True;

                //把登录数据保存在JSON字符串中,以便写入更多数据, 如id等
                joUser  := _json('{}');
                joUser.uid      := iUid;
                joUser.name     := sName;
                joUser.logindate:= dwDateToPHPDate(Now);
                sInfo   := joUser;

                //写入cookie. 对写入值进行加密
                sCookie := joConfig.cookie._(0);
                sValue  := joConfig.cookie._(1);
                sValue  := dwAESEncrypt(sInfo,sValue);
                dwSetCookie(self,sCookie,sValue,30*24);
                dwSetCookiePro(self,sCookie,sValue,'/','.delphibbs.com',30*24);

                //下面是检查验证码. 验证码可点击自动切换
                dwOpenUrl(self,joConfig.successurl,'_self');
            end;
        end;

        //
        if not bLogin then begin
            dwMessage(joConfig.error,'error',self);
        end;
    except
        dwMessage('配置信息不正确,请检查!','error',self);
        Exit;
    end;

end;

end.
