﻿unit unit1;

interface

uses
     dwBase,
     SynCommons,

     //
     Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
     Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Data.DB,
  Data.Win.ADODB, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, Vcl.Samples.Spin;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    Panel_0_Banner: TPanel;
    Label3: TLabel;
    Panel_Title: TPanel;
    Label_Title: TLabel;
    Panel_Options: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    SpinEdit_radius: TSpinEdit;
    SpinEdit_activeRadius: TSpinEdit;
    SpinEdit_LineWidth: TSpinEdit;
    ComboBox_digitalFlopUnit: TComboBox;
    SpinEdit_activeTimeGap: TSpinEdit;
    Label14: TLabel;
    SpinEdit_FontSize: TSpinEdit;
    Label15: TLabel;
    SpinEdit_digitalFlopToFixed: TSpinEdit;
    ComboBox_showOriginValue: TComboBox;
    Label18: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
        Procedure UpdateOptions(Sender: TObject);
    procedure SpinEdit_FontSizeChange(Sender: TObject);
  private
    { Private declarations }
  public
  end;

var
     Form1             : TForm1;


implementation


{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
    iRow    : Integer;
begin
    with StringGrid1 do begin
        iRow    := 0;
        Cells[0,iRow]   := '北京';  Cells[1,iRow]   := '55';    Cells[2,iRow]   := '#FF5D1D';   Inc(iRow);
        Cells[0,iRow]   := '上海';  Cells[1,iRow]   := '98 ';   Cells[2,iRow]   := '#FFA91F';   Inc(iRow);
        Cells[0,iRow]   := '广州';  Cells[1,iRow]   := '76';    Cells[2,iRow]   := '#9DF90D';   Inc(iRow);
        Cells[0,iRow]   := '深圳';  Cells[1,iRow]   := '83';    Cells[2,iRow]   := '#B283FC';   Inc(iRow);
    end;
end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    Docksite    := True;
end;

procedure TForm1.SpinEdit_FontSizeChange(Sender: TObject);
begin
    StringGrid1.Font.Size   := SpinEdit_FontSize.Value;
    UpdateOptions(nil);
end;

procedure TForm1.UpdateOptions(Sender: TObject);
var
    joHint  : Variant;
begin
    //取得Hint属性对象
    joHint  := _json(StringGrid1.Hint);
    if joHint = unassigned then begin
        joHint  := _json('{}');
    end;

    //radius	环半径	String|Number	'50%'|100	'50%'
    joHint.baseradius   := SpinEdit_radius.Value;

    //activeRadius	环半径（动态）	String|Number	'55%'|110	'55%'
    joHint.activeRadius := SpinEdit_activeRadius.Value;

    //data	环数据	Array<Object>	data属性	[]

    //lineWidth	环线条宽度	Number	---	20
    joHint.lineWidth := SpinEdit_lineWidth.Value;

    //activeTimeGap	切换间隔(ms)	Number	---	3000
    joHint.activeTimeGap := SpinEdit_activeTimeGap.Value*1000;

    //color	环颜色	Array<String>	[1]	[]

    //digitalFlopStyle	数字翻牌器样式	Object	---	[2]
    joHint.digitalFlopStyle := '{fontSize: '+IntToStr(SpinEdit_FontSize.Value)+'}';

    //digitalFlopToFixed	数字翻牌器小数位数	Number	---	0
    joHint.digitalFlopToFixed := SpinEdit_digitalFlopToFixed.Value;

    //digitalFlopUnit	数字翻牌器单位	String	---	''
    joHint.digitalFlopUnit    := ComboBox_digitalFlopUnit.Text;

    //showOriginValue	显示原始值	Boolean	---	false
    joHint.showOriginValue    := ComboBox_showOriginValue.ItemIndex=0;



    //
    StringGrid1.Hint    := joHint;
end;

end.
