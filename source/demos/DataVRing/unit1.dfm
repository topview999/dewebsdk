object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'DeWeb DataV Ring'
  ClientHeight = 675
  ClientWidth = 625
  Color = 2954240
  TransparentColor = True
  TransparentColorValue = 2954240
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnMouseDown = FormMouseDown
  PixelsPerInch = 96
  TextHeight = 20
  object StringGrid1: TStringGrid
    AlignWithMargins = True
    Left = 0
    Top = 110
    Width = 625
    Height = 319
    HelpType = htKeyword
    HelpKeyword = 'dvring'
    Margins.Left = 0
    Margins.Top = 60
    Margins.Right = 0
    Align = alTop
    BevelOuter = bvNone
    BorderStyle = bsNone
    ColCount = 2
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitTop = 30
    RowHeights = (
      24
      24
      24
      24
      24)
  end
  object Panel_0_Banner: TPanel
    Left = 0
    Top = 0
    Width = 625
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    ParentBackground = False
    ParentColor = True
    TabOrder = 1
    ExplicitLeft = -8
    ExplicitTop = -18
    object Label3: TLabel
      AlignWithMargins = True
      Left = 210
      Top = 3
      Width = 367
      Height = 44
      Margins.Left = 10
      Align = alLeft
      AutoSize = False
      Caption = 'DataV Ring'
      Color = 4210752
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      ExplicitLeft = 226
      ExplicitTop = -13
    end
    object Panel_Title: TPanel
      Left = 0
      Top = 0
      Width = 200
      Height = 50
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -29
      Font.Name = 'Verdana'
      Font.Style = [fsBold, fsItalic]
      ParentBackground = False
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object Label_Title: TLabel
        Left = 0
        Top = 0
        Width = 200
        Height = 50
        HelpType = htKeyword
        Align = alClient
        Alignment = taCenter
        Caption = 'DeWeb'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Layout = tlCenter
        WordWrap = True
        ExplicitWidth = 96
        ExplicitHeight = 29
      end
    end
  end
  object Panel_Options: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 435
    Width = 619
    Height = 237
    HelpKeyword = 'dvbox'
    HelpContext = 11
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Options'
    Font.Charset = ANSI_CHARSET
    Font.Color = 13158600
    Font.Height = -15
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    VerticalAlignment = taAlignTop
    ExplicitLeft = 322
    ExplicitTop = 80
    ExplicitWidth = 650
    ExplicitHeight = 525
    object Label1: TLabel
      Left = 36
      Top = 81
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'radius'
    end
    object Label2: TLabel
      Left = 308
      Top = 78
      Width = 140
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'digitalFlopToFixed'
    end
    object Label4: TLabel
      Left = 308
      Top = 117
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'digitalFlopUnit'
    end
    object Label6: TLabel
      Left = 36
      Top = 117
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'activeRadius'
    end
    object Label7: TLabel
      Left = 36
      Top = 153
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'lineWidth'
    end
    object Label14: TLabel
      Left = 36
      Top = 189
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'activeTimeGap'
    end
    object Label15: TLabel
      Left = 308
      Top = 149
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'fontSize'
    end
    object Label18: TLabel
      Left = 308
      Top = 187
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'showOriginValue'
    end
    object SpinEdit_radius: TSpinEdit
      Left = 182
      Top = 80
      Width = 100
      Height = 30
      Hint = '{"dwstyle":"border:solid 1px #eee;border-radius:5px;"}'
      HelpContext = 10
      MaxValue = 80
      MinValue = 20
      TabOrder = 0
      Value = 50
      OnChange = UpdateOptions
    end
    object SpinEdit_activeRadius: TSpinEdit
      Left = 182
      Top = 116
      Width = 100
      Height = 30
      Hint = '{"dwstyle":"border:solid 1px #eee;border-radius:5px;"}'
      HelpContext = 10
      MaxValue = 90
      MinValue = 20
      TabOrder = 1
      Value = 55
      OnChange = UpdateOptions
    end
    object SpinEdit_LineWidth: TSpinEdit
      Left = 182
      Top = 152
      Width = 100
      Height = 30
      Hint = '{"dwstyle":"border:solid 1px #eee;border-radius:5px;"}'
      HelpContext = 10
      MaxValue = 50
      MinValue = 10
      TabOrder = 2
      Value = 20
      OnChange = UpdateOptions
    end
    object ComboBox_digitalFlopUnit: TComboBox
      Left = 454
      Top = 114
      Width = 100
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 3
      OnChange = UpdateOptions
      Items.Strings = (
        ''
        #21488
        #37096
        #20010
        #21482)
    end
    object SpinEdit_activeTimeGap: TSpinEdit
      Left = 182
      Top = 188
      Width = 100
      Height = 30
      Hint = '{"dwstyle":"border:solid 1px #eee;border-radius:5px;"}'
      HelpContext = 10
      MaxValue = 8
      MinValue = 1
      TabOrder = 4
      Value = 3
      OnChange = UpdateOptions
    end
    object SpinEdit_FontSize: TSpinEdit
      Left = 454
      Top = 148
      Width = 100
      Height = 30
      Hint = '{"dwstyle":"border:solid 1px #eee;border-radius:5px;"}'
      HelpContext = 10
      MaxValue = 50
      MinValue = 6
      TabOrder = 5
      Value = 20
      OnChange = SpinEdit_FontSizeChange
    end
    object SpinEdit_digitalFlopToFixed: TSpinEdit
      Left = 454
      Top = 78
      Width = 100
      Height = 30
      Hint = '{"dwstyle":"border:solid 1px #eee;border-radius:5px;"}'
      HelpContext = 10
      MaxValue = 4
      MinValue = 0
      TabOrder = 6
      Value = 0
      OnChange = UpdateOptions
    end
    object ComboBox_showOriginValue: TComboBox
      Left = 454
      Top = 184
      Width = 100
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 1
      TabOrder = 7
      Text = 'false'
      OnChange = UpdateOptions
      Items.Strings = (
        'true'
        'false')
    end
  end
end
