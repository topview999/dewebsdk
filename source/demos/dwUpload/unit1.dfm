object Form1: TForm1
  Left = 0
  Top = 0
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 558
  ClientWidth = 800
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = 16448250
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnEndDock = FormEndDock
  OnStartDock = FormStartDock
  PixelsPerInch = 96
  TextHeight = 21
  object Label1: TLabel
    AlignWithMargins = True
    Left = 10
    Top = 237
    Width = 780
    Height = 21
    Margins.Left = 10
    Margins.Top = 20
    Margins.Right = 10
    Margins.Bottom = 0
    Align = alTop
    Caption = 'Accept ('#25991#20214#19978#20256#36827#34892#25552#20132#30340#25991#20214#31867#22411')'
    ExplicitWidth = 275
  end
  object Label3: TLabel
    AlignWithMargins = True
    Left = 10
    Top = 327
    Width = 780
    Height = 21
    Margins.Left = 10
    Margins.Top = 20
    Margins.Right = 10
    Margins.Bottom = 0
    Align = alTop
    Caption = #19978#20256#25991#20214#30446#24405
    ExplicitWidth = 96
  end
  object Label4: TLabel
    Left = 200
    Top = 480
    Width = 265
    Height = 21
    Alignment = taCenter
    AutoSize = False
    Caption = 'waiting...'
  end
  object Button1: TButton
    AlignWithMargins = True
    Left = 10
    Top = 407
    Width = 780
    Height = 35
    Hint = '{"type":"success"}'
    Margins.Left = 10
    Margins.Top = 10
    Margins.Right = 10
    Margins.Bottom = 10
    Align = alTop
    Caption = 'Upload'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindow
    Font.Height = -16
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit_Dir: TEdit
    AlignWithMargins = True
    Left = 10
    Top = 358
    Width = 780
    Height = 29
    Margins.Left = 10
    Margins.Top = 10
    Margins.Right = 10
    Margins.Bottom = 10
    Align = alTop
    TabOrder = 1
    Text = 'MyDir'
  end
  object Panel_Title: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 217
    Align = alTop
    ParentBackground = False
    TabOrder = 2
    object Label2: TLabel
      AlignWithMargins = True
      Left = 11
      Top = 11
      Width = 778
      Height = 195
      Margins.Left = 10
      Margins.Top = 10
      Margins.Right = 10
      Margins.Bottom = 10
      Align = alClient
      Caption = 
        '<b>dwUpload'#20989#25968'</b><hr>'#21442#25968':'#13'<li>AForm : '#31383#20307#26412#36523#65292#19968#33324#29992'self</li><li>AAccep' +
        't : '#25991#20214#19978#20256#31867#22411#25511#21046#65292#31867#20284'"image/gif, image/jpeg"</li><li>ADestDir : '#19978#20256#30446#24405#65292#20026 +
        #31354#26102#19978#20256#21040#26381#21153#22120'upload'#30446#24405#65292#26377#20540#26102#19978#20256#21040#25351#23450#30446#24405#65292#25903#25345#23376#30446#24405'</li>'#20107#20214':'#13#19978#20256#23436#25104#21518#65292#33258#21160#35302#21457#31383#20307#30340'OnEndDock' +
        #20107#20214
      ExplicitWidth = 1567
      ExplicitHeight = 63
    end
  end
  object ComboBox_Accept: TComboBox
    AlignWithMargins = True
    Left = 10
    Top = 268
    Width = 780
    Height = 29
    Margins.Left = 10
    Margins.Top = 10
    Margins.Right = 10
    Margins.Bottom = 10
    Align = alTop
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 3
    Text = 'image/*'
    Items.Strings = (
      'image/*'
      'video/*'
      'image/gif, image/jpeg'
      '.doc,.docx,.xls,.xlsx,.pdf'
      'application/msword'
      'application/pdf'
      'application/poscript'
      'application/rtf'
      'application/x-zip-compressed'
      'audio/basic'
      'audio/x-aiff'
      'audio/x-mpeg'
      'audio/x-pn/realaudio'
      'audio/x-waw'
      'image/gif'
      'image/jpeg'
      'image/tiff'
      'image/x-ms-bmp'
      'image/x-photo-cd'
      '.dll')
  end
end
