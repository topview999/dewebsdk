﻿unit unit1;

interface

uses
     //
     dwBase,

     //
     Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
     Vcl.Controls, Vcl.Forms, Vcl.StdCtrls, Vcl.Grids, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Menus,
  Vcl.Buttons, Data.DB, Data.Win.ADODB, Vcl.Samples.Spin;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Edit_Dir: TEdit;
    Panel_Title: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    ComboBox_Accept: TComboBox;
    Label4: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormEndDock(Sender, Target: TObject; X, Y: Integer);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}



procedure TForm1.Button1Click(Sender: TObject);
var
    sJS : String;
begin
    //var sJS := 'let oInp=document.getElementById('''+Self.Name+'__inp'');this.dw_upload__dir=''new1'';'
    //        +'oInp.accept=''image/png'';oInp.click();';
    //dwRunJS(sJS,self);

    //
    dwUpload(self,ComboBox_Accept.Text,Edit_Dir.Text);

    //sJS := 'this.dwInputClick(\"'+Self.Name+'\");';
    //dwRunJS(sJS,self);
end;

procedure TForm1.FormEndDock(Sender, Target: TObject; X, Y: Integer);
var
    sFile   : string;
begin
    //上传完成时激活事件

    //
    sFile   := dwGetProp(self,'__upload');
    //
    dwShowMessage('Upload success!  '+sFile,self);
end;

procedure TForm1.FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
begin
    //开始上传时激活事件
    dwShowMessage(dwGetProp(Self,'interactionmethod'),self); //__upload_start
end;

end.
