﻿unit unit1;

interface

uses
    //
    dwBase,
    CloneComponents,


    //
    Math,
    Graphics,strutils,
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.Buttons,  Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    SB: TScrollBox;
    Panel2: TPanel;
    Button18: TButton;
    Button19: TButton;
    Button20: TButton;
    Button21: TButton;
    Button22: TButton;
    Button23: TButton;
    Button24: TButton;
    Button25: TButton;
    Button26: TButton;
    Button27: TButton;
    Button28: TButton;
    Button29: TButton;
    Button30: TButton;
    Button31: TButton;
    Button32: TButton;
    Button33: TButton;
    Button34: TButton;
    Button_Set: TButton;
    Button_Get: TButton;
    Label1: TLabel;
    Button1: TButton;
    ScrollBox1: TScrollBox;
    Panel1: TPanel;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    Button13: TButton;
    Button14: TButton;
    Button15: TButton;
    Button16: TButton;
    Button17: TButton;
    Button35: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    procedure Button_SetClick(Sender: TObject);
    procedure Button_GetClick(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
    procedure SBMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.Button_SetClick(Sender: TObject);
begin
    //dwRunJS('this.$refs[''SB''].wrap.scrollTop = 200;',self);
    dwSetScroll(SB,300);
end;

procedure TForm1.Button1Click(Sender: TObject);
var
    I       : Integer;
    oButton : TButton;
begin

    //以下为网友做的克隆控件测试
    //
    for I := Panel2.ControlCount-1 downto 0 do begin
        if Panel2.Controls[I].Name <> 'Button34' then begin
            Panel2.Controls[I].Destroy;
        end;
    end;
    for I := 2 to 30 do begin
        oButton := TButton(CloneComponent(Button34));
        oButton.Name    := 'Button_'+IntToStr(I);
        oButton.Parent  := Panel2;
        oButton.Caption := IntToStr(Random(1000));
        oButton.Top     := I * 1000;
    end;
    //
    Panel2.Height   := 30 * (Button34.Height+6);
    //
    DockSite    := True;
end;

procedure TForm1.Button_GetClick(Sender: TObject);
begin
    dwMessage('current scroll : '+IntToStr(SB.HelpContext),'success',self);
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    dwSetMobileMode(self,480,960);
end;

procedure TForm1.SBMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    Label1.Caption  := IntToStr(X);
end;

end.
