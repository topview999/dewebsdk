object Form1: TForm1
  Left = 0
  Top = 0
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DBControl'
  ClientHeight = 640
  ClientWidth = 1000
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 21
  object Panel_0_Banner: TPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = 4271650
    ParentBackground = False
    TabOrder = 0
    object Label3: TLabel
      AlignWithMargins = True
      Left = 210
      Top = 3
      Width = 163
      Height = 44
      Margins.Left = 10
      Align = alLeft
      Caption = 'DataBase Controls'
      Color = 4210752
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      ExplicitHeight = 18
    end
    object Panel_Title: TPanel
      Left = 0
      Top = 0
      Width = 200
      Height = 50
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -29
      Font.Name = 'Verdana'
      Font.Style = [fsBold, fsItalic]
      ParentBackground = False
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object Label_Title: TLabel
        Left = 0
        Top = 0
        Width = 200
        Height = 50
        HelpType = htKeyword
        Align = alClient
        Alignment = taCenter
        Caption = 'DeWeb'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Layout = tlCenter
        WordWrap = True
        ExplicitWidth = 96
        ExplicitHeight = 29
      end
    end
    object Edit_Search: TEdit
      AlignWithMargins = True
      Left = 386
      Top = 11
      Width = 287
      Height = 27
      Hint = 
        '{"placeholder":"'#35831#36755#20837#26597#35810#20851#38190#23383'","radius":"15px","suffix-icon":"el-icon' +
        '-search","dwstyle":"padding-left:10px;"}'
      Margins.Left = 10
      Margins.Top = 11
      Margins.Right = 1
      Margins.Bottom = 12
      Align = alLeft
      TabOrder = 1
      OnChange = Edit_SearchChange
      ExplicitHeight = 29
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 50
    Width = 1000
    Height = 441
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    ParentBackground = False
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 736
      Height = 441
      Hint = '{"dwattr":"stripe"}'
      HelpType = htKeyword
      HelpKeyword = 'ww'
      HelpContext = 40
      Align = alClient
      DataSource = DataSource1
      Font.Charset = ANSI_CHARSET
      Font.Color = 6579300
      Font.Height = -16
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -16
      TitleFont.Name = #24494#36719#38597#40657
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'AName'
          Title.Caption = #22995#21517
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sex'
          Title.Caption = #24615#21035
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Province'
          Title.Caption = #31821#36143
          Width = 63
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'HeadShip'
          Title.Caption = #32844#21153
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Birthday'
          Title.Caption = #20986#29983#26085#26399
          Width = 115
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Addr'
          Title.Caption = #36890#20449#22320#22336
          Width = 270
          Visible = True
        end>
    end
    object Panel2: TPanel
      Left = 736
      Top = 0
      Width = 264
      Height = 441
      Hint = '{"dwstyle":"border:solid 1px #eee;"}'
      Align = alRight
      BevelOuter = bvNone
      Color = 16448250
      Font.Charset = ANSI_CHARSET
      Font.Color = 6579300
      Font.Height = -16
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 18
        Width = 32
        Height = 21
        Caption = #22995#21517
      end
      object Label2: TLabel
        Left = 16
        Top = 141
        Width = 32
        Height = 21
        Caption = #32844#21153
      end
      object Label4: TLabel
        Left = 16
        Top = 58
        Width = 32
        Height = 21
        Caption = #24615#21035
      end
      object Label6: TLabel
        Left = 16
        Top = 98
        Width = 32
        Height = 21
        Caption = #31821#36143
      end
      object Label7: TLabel
        Left = 16
        Top = 185
        Width = 32
        Height = 21
        Caption = #29983#26085
      end
      object Label8: TLabel
        Left = 16
        Top = 227
        Width = 32
        Height = 21
        Caption = #22320#22336
      end
      object Button_Save: TButton
        AlignWithMargins = True
        Left = 10
        Top = 396
        Width = 244
        Height = 35
        Hint = '{"type":"primary"}'
        Margins.Left = 10
        Margins.Top = 10
        Margins.Right = 10
        Margins.Bottom = 10
        Align = alBottom
        Caption = #20445#23384
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = Button_SaveClick
      end
      object Edit_AName: TEdit
        Left = 64
        Top = 15
        Width = 121
        Height = 29
        TabOrder = 1
        Text = 'Edit_AName'
      end
      object CheckBox_Sex: TCheckBox
        Left = 64
        Top = 61
        Width = 97
        Height = 17
        TabOrder = 2
      end
      object ComboBox_Province: TComboBox
        Left = 64
        Top = 95
        Width = 145
        Height = 29
        TabOrder = 3
        Text = 'ComboBox_Province'
        Items.Strings = (
          #21271#20140
          #19978#28023
          #24191#19996
          #28246#21335
          #28246#21271
          #38485#35199
          #22235#24029
          #27743#35199)
      end
      object Edit_Headship: TEdit
        Left = 64
        Top = 138
        Width = 121
        Height = 29
        TabOrder = 4
        Text = 'Edit1'
      end
      object DateTimePicker_Birthday: TDateTimePicker
        Left = 64
        Top = 185
        Width = 186
        Height = 29
        Date = 44543.000000000000000000
        Time = 0.936194432870252100
        TabOrder = 5
      end
      object Memo_Addr: TMemo
        Left = 64
        Top = 227
        Width = 185
        Height = 89
        Lines.Strings = (
          'Memo_Addr')
        TabOrder = 6
      end
    end
  end
  object ADOQuery1: TADOQuery
    CursorType = ctStatic
    AfterScroll = ADOQuery1AfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM dw_Member')
    Left = 496
    Top = 480
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 400
    Top = 480
  end
end
