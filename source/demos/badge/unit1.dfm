object Form1: TForm1
  Left = 0
  Top = 0
  HelpType = htKeyword
  Margins.Top = 20
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 541
  ClientWidth = 630
  Color = clBtnFace
  TransparentColor = True
  TransparentColorValue = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -17
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 23
  object Panel1: TPanel
    Left = 96
    Top = 152
    Width = 77
    Height = 37
    HelpType = htKeyword
    HelpKeyword = 'badge'
    AutoSize = True
    Caption = '12'
    TabOrder = 0
    object Button1: TButton
      Left = 1
      Top = 1
      Width = 75
      Height = 35
      Caption = #28857#36190
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 96
    Top = 208
    Width = 77
    Height = 37
    Hint = '{"type":"info"}'
    HelpType = htKeyword
    HelpKeyword = 'badge'
    AutoSize = True
    Caption = '12'
    TabOrder = 1
    object Button2: TButton
      Left = 1
      Top = 1
      Width = 75
      Height = 35
      Caption = #35780#35770
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 95
    Top = 264
    Width = 77
    Height = 37
    HelpType = htKeyword
    HelpKeyword = 'badge'
    AutoSize = True
    Caption = '99+'
    TabOrder = 2
    object Button3: TButton
      Left = 1
      Top = 1
      Width = 75
      Height = 35
      Caption = #25910#34255' '
      TabOrder = 0
    end
  end
  object Panel4: TPanel
    Left = 94
    Top = 320
    Width = 77
    Height = 37
    Hint = '{"dwattr":"is-dot"}'
    HelpType = htKeyword
    HelpKeyword = 'badge'
    AutoSize = True
    Caption = '128'
    TabOrder = 3
    object Button4: TButton
      Left = 1
      Top = 1
      Width = 75
      Height = 35
      Caption = #36716#21457
      TabOrder = 0
    end
  end
  object Panel5: TPanel
    Left = 96
    Top = 90
    Width = 77
    Height = 37
    HelpType = htKeyword
    HelpKeyword = 'badge'
    Caption = '12'
    TabOrder = 4
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 75
      Height = 35
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Caption = #32508#21512#26696#20363
      Layout = tlCenter
      ExplicitWidth = 68
      ExplicitHeight = 23
    end
  end
  object Panel6: TPanel
    Left = 97
    Top = 375
    Width = 77
    Height = 37
    Hint = '{"type":"text"}'
    HelpType = htKeyword
    HelpKeyword = 'badge'
    AutoSize = True
    Caption = '12'
    TabOrder = 5
    object Button5: TButton
      Left = 1
      Top = 1
      Width = 75
      Height = 35
      Caption = #35780#35770
      TabOrder = 0
    end
  end
end
