﻿unit unit1;

interface

uses
     //
     dwBase,

     //
     Math,
     Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
     Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.MPlayer,
     Vcl.Menus, Vcl.Buttons, Vcl.Samples.Spin, Vcl.Imaging.jpeg,
     Vcl.Imaging.pngimage, VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.Series,
  VCLTee.TeeProcs, VCLTee.Chart;

type
  TForm1 = class(TForm)
    Panel_02_Content: TPanel;
    Chart1: TChart;
    Chart2: TChart;
    Series5: TBarSeries;
    Series6: TBarSeries;
    Chart4: TChart;
    Series7: TPieSeries;
    Series1: TLineSeries;
    Series2: TLineSeries;
    Panel_01_Tile: TPanel;
    Label1: TLabel;
    Button_Update: TButton;
    Chart3: TChart;
    Series3: THorizBarSeries;
    Series4: THorizBarSeries;
    Chart5: TChart;
    Series8: TAreaSeries;
    Series9: TAreaSeries;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button_UpdateClick(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.Button_UpdateClick(Sender: TObject);
var
     I    : Integer;
const
     _SS  : array[0..9] of String=('新','年','快','乐','心','想','事','成','！','！！');
     _SS1 : array[0..9] of String=('一月','二月','三月','四月','五月','六月','七月','八月','九月','十月');
begin

     //
     Randomize;
     Series1.Clear;
     Series2.Clear;
     Series3.Clear;
     Series4.Clear;
     Series5.Clear;
     Series6.Clear;
     Series7.Clear;
     Series8.Clear;
     Series9.Clear;
     for I:= 0 to 9 do begin
          Series1.AddY(Random(100),_SS[I]);
          Series2.AddY(Random(110));
          Series3.AddY(Random(100));
          Series4.AddY(Random(100));
          Series5.AddY(Random(100));
          Series6.AddY(Random(100));
          Series8.AddY(Random(100),_SS1[I]);
          Series9.AddY(Random(100),_SS1[I]);
          //
          if I<4 then begin
               Series7.AddPie(Random(100),'二程'+IntToStr(I+1)+'部',Random($FFFFFF));
          end;
     end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
     Button_Update.OnClick(self);
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton;  Shift: TShiftState; X, Y: Integer);
begin
    //设置当前屏幕显示模式为移动应用模式.如果电脑访问，则按414x726（iPhone6/7/8 plus）显示
    dwBase.dwSetMobileMode(self,414,736);
    //
    Height  := Max(1620,Y);
end;

end.
