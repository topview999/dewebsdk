object Form1: TForm1
  Left = 0
  Top = 0
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 661
  ClientWidth = 900
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 20
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 900
    Height = 65
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'DeWeb '#36208#39532#28783#23637#31034' '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = #24494#36719#38597#40657
    Font.Style = [fsBold]
    ParentFont = False
    Layout = tlCenter
    ExplicitWidth = 320
  end
  object Image6: TImage
    AlignWithMargins = True
    Left = 228
    Top = 390
    Width = 450
    Height = 223
    Hint = '{"src":"media/images/carouselcard/1.jpg"}'
    Margins.Left = 4
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    IncrementalDisplay = True
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 65
    Width = 900
    Height = 310
    Hint = '{"dwattr":"type='#39'card'#39'"}'
    HelpType = htKeyword
    HelpKeyword = 'carousel'
    ActivePage = TabSheet1
    Align = alTop
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      TabVisible = False
      object Image1: TImage
        AlignWithMargins = True
        Left = 4
        Top = 0
        Width = 450
        Height = 300
        Hint = '{"src":"media/images/carouselcard/1.jpg"}'
        Margins.Left = 4
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        OnClick = Image1Click
        ExplicitLeft = 0
        ExplicitHeight = 298
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      TabVisible = False
      object Image2: TImage
        AlignWithMargins = True
        Left = 4
        Top = 0
        Width = 450
        Height = 300
        Hint = '{"src":"media/images/carouselcard/2.jpg"}'
        Margins.Left = 4
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 12
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      TabVisible = False
      object Image3: TImage
        AlignWithMargins = True
        Left = 4
        Top = 0
        Width = 450
        Height = 300
        Hint = '{"src":"media/images/carouselcard/3.jpg"}'
        Margins.Left = 4
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 12
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
      TabVisible = False
      object Image4: TImage
        AlignWithMargins = True
        Left = 4
        Top = 0
        Width = 450
        Height = 300
        Hint = '{"src":"media/images/carouselcard/4.jpg"}'
        Margins.Left = 4
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 12
        ExplicitHeight = 275
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'TabSheet5'
      ImageIndex = 4
      TabVisible = False
      object Image5: TImage
        AlignWithMargins = True
        Left = 4
        Top = 0
        Width = 450
        Height = 300
        Hint = '{"src":"media/images/carouselcard/5.jpg"}'
        Margins.Left = 4
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 12
        ExplicitHeight = 275
      end
    end
  end
  object Button1: TButton
    Left = 40
    Top = 448
    Width = 81
    Height = 41
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
end
