﻿unit unit1;

interface

uses
     //
     dwBase,

     //
     Math,
     Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
     Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.Menus, VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart, Vcl.Imaging.pngimage;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure Image1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.Button1Click(Sender: TObject);
begin
    dwRunJS('this.image_preview_list=[];this.image_preview_list.push(''media/images/logo.png'');',self);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
    Image1.IncrementalDisplay   := False;
    Image2.IncrementalDisplay   := Image1.IncrementalDisplay;
    Image3.IncrementalDisplay   := Image1.IncrementalDisplay;
    Image4.IncrementalDisplay   := Image1.IncrementalDisplay;
    Image5.IncrementalDisplay   := Image1.IncrementalDisplay;
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,  Y: Integer);
var
    iTab    : Integer;
begin
    //dwSetMobileMode(self,900,360);

    //调整宽度，以解决TabSheet自带4像素边框的问题
    for iTab := 0 to PageControl1.PageCount-1 do begin
        //TPanel(PageControl1.Pages[iTab].Controls[0]).Width  := Width-8;
    end;
end;

procedure TForm1.Image1Click(Sender: TObject);
begin
    dwRunJS('this.image_preview_list=[];this.image_preview_list.push(''media/images/logo.png'');',self);
end;

end.
