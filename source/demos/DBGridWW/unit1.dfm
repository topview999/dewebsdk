object Form1: TForm1
  Left = 0
  Top = 0
  HelpType = htKeyword
  Margins.Top = 20
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DBGrid - WestWind'
  ClientHeight = 725
  ClientWidth = 1000
  Color = clWindow
  TransparentColor = True
  TransparentColorValue = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -17
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnMouseUp = FormMouseUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 23
  object Label_Selections: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 688
    Width = 994
    Height = 17
    Align = alTop
    Alignment = taCenter
    Caption = '...'
    Font.Charset = ANSI_CHARSET
    Font.Color = 9868950
    Font.Height = -12
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentFont = False
    Layout = tlCenter
    WordWrap = True
    ExplicitWidth = 9
  end
  object DBGrid1: TDBGrid
    AlignWithMargins = True
    Left = 0
    Top = 50
    Width = 999
    Height = 587
    Hint = 
      '{"merge":[[3,5,8,"'#22522#26412#20449#24687'"],[2,7,8,"'#36890#20449#22320#22336'"]],"rowheight":40,"summary' +
      '":["'#27719#24635'",[6,"sum","'#21512#35745#65306'%.0f"],[6,"avg","'#24179#22343#65306'%.0f"],[9,"max","'#26368#22823#65306'%.0' +
      'f%%"],[9,"min","'#26368#23567#65306'%.0f%%"]]}'
    HelpType = htKeyword
    HelpKeyword = 'ww'
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 1
    Margins.Bottom = 0
    Align = alTop
    Ctl3D = True
    DataSource = DataSource1
    FixedColor = 16316664
    Font.Charset = ANSI_CHARSET
    Font.Color = 9868950
    Font.Height = -15
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    Options = [dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -17
    TitleFont.Name = #24494#36719#38597#40657
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnEndDock = DBGrid1EndDock
    Columns = <
      item
        Expanded = False
        Title.Caption = '{"type": "check"}'
        Width = 32
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = '{"type": "index","caption":"'#24207#21495'"}'
        Width = 32
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 
          '{"type":"image","caption":"'#30456#29255'","fieldname":"photo","format":"med' +
          'ia/images/mm/%s.jpg","dwstyle":"border:solid 1px #ddd;border-rad' +
          'ius:8px;width:32px;height:32px;fill:fill;"}'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        ReadOnly = True
        Title.Caption = '{"fieldname":"aname","caption":"'#22995#21517'","srot":1}'
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 
          '{"caption": "'#24615#21035'","type": "boolean","list": ["'#27721#23376'",'#10'"'#32654#22899'"'#10'],"fieldn' +
          'ame": "sex","sort":1}'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 
          '{"caption":"'#20986#29983#26085#26399'","fieldname":"birthday","type":"date","format":' +
          '"yyyy-MM-dd"}'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 
          '{"type":"float","format":"%n","caption":"'#24037#36164'","fieldname":"salary' +
          '","sort":1,"align":"right"}'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 
          '{"caption":"'#30465#20221'","fieldname":"province","color":"#88c","bkcolor":' +
          '"#fafafa","list":["'#21271#20140'","'#19978#28023'","'#22825#27941'","'#28246#21271'","'#27827#21271'","'#24191#19996'","'#23665#19996'","'#38485#35199'","'#28246#21335'","' +
          #24191#35199'"],"sort":1}'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = '{"caption":"'#22320#22336'","fieldname":"addr","align":"left","sort":1}'
        Width = 246
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 
          '{"type":"progress","caption":"'#24037#20316#36827#24230'","fieldname":"progress","tota' +
          'l":100}'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 
          '{"type":"button","caption":"'#25805#20316'","list":[["'#23457#26680'","primary"],["'#26597#30475'","' +
          'success"],["'#21024#38500'","danger"]]}'
        Width = 150
        Visible = True
      end>
  end
  object Panel_0_Banner: TPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = 4271650
    ParentBackground = False
    TabOrder = 1
    object Label_Name: TLabel
      AlignWithMargins = True
      Left = 147
      Top = 3
      Width = 183
      Height = 44
      Margins.Left = 10
      Align = alLeft
      AutoSize = False
      Caption = 'DBGrid - WestWind'
      Color = 4210752
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      ExplicitLeft = 210
    end
    object Panel_Title: TPanel
      Left = 0
      Top = 0
      Width = 137
      Height = 50
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -29
      Font.Name = 'Verdana'
      Font.Style = [fsBold, fsItalic]
      ParentBackground = False
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object Label_Title: TLabel
        Left = 0
        Top = 0
        Width = 137
        Height = 50
        HelpType = htKeyword
        Align = alClient
        Alignment = taCenter
        Caption = 'DeWeb'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Layout = tlCenter
        WordWrap = True
        ExplicitWidth = 96
        ExplicitHeight = 29
      end
    end
    object Edit_Search: TEdit
      AlignWithMargins = True
      Left = 343
      Top = 11
      Width = 287
      Height = 27
      Hint = 
        '{"placeholder":"'#35831#36755#20837#26597#35810#20851#38190#23383'","radius":"15px","suffix-icon":"el-icon' +
        '-search","dwstyle":"padding-left:10px;"}'
      Margins.Left = 10
      Margins.Top = 11
      Margins.Right = 1
      Margins.Bottom = 12
      Align = alLeft
      TabOrder = 1
      OnChange = Edit_SearchChange
      ExplicitHeight = 31
    end
    object Button_ToExcel: TButton
      AlignWithMargins = True
      Left = 897
      Top = 10
      Width = 93
      Height = 30
      Hint = 
        '{"type":"primary","icon":"el-icon-right","onclick":"this.dwloadi' +
        'ng=true;"}'
      Margins.Top = 10
      Margins.Right = 10
      Margins.Bottom = 10
      Align = alRight
      Caption = 'Excel'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = Button_ToExcelClick
    end
    object Button_Edit: TButton
      AlignWithMargins = True
      Left = 791
      Top = 10
      Width = 93
      Height = 30
      Hint = '{"type":"primary","icon":"el-icon-edit-outline"}'
      Margins.Top = 10
      Margins.Right = 10
      Margins.Bottom = 10
      Align = alRight
      Caption = 'Edit'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = Button_EditClick
    end
    object Button_Print: TButton
      AlignWithMargins = True
      Left = 685
      Top = 10
      Width = 93
      Height = 30
      Hint = '{"type":"primary","icon":"el-icon-printer"}'
      Margins.Top = 10
      Margins.Right = 10
      Margins.Bottom = 10
      Align = alRight
      Caption = 'Print'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = Button_PrintClick
    end
  end
  object TrackBar1: TTrackBar
    AlignWithMargins = True
    Left = 3
    Top = 647
    Width = 994
    Height = 35
    Hint = '{"dwattr":"background layout='#39'prev, pager, next, jumper'#39'"}'
    HelpType = htKeyword
    HelpKeyword = 'wwpage'
    Margins.Top = 10
    Align = alTop
    PageSize = 10
    TabOrder = 2
    OnChange = TrackBar1Change
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 72
    Top = 448
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 168
    Top = 448
  end
end
