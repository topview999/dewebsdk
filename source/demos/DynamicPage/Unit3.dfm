object Form3: TForm3
  Left = 0
  Top = 0
  HelpType = htKeyword
  HelpKeyword = 'embed'
  Align = alClient
  BorderStyle = bsNone
  Caption = 'Form3'
  ClientHeight = 281
  ClientWidth = 488
  Color = clAppWorkSpace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 20
    Top = 20
    Width = 448
    Height = 241
    Margins.Left = 20
    Margins.Top = 20
    Margins.Right = 20
    Margins.Bottom = 20
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    ExplicitLeft = 32
    ExplicitTop = 25
    ExplicitWidth = 185
    ExplicitHeight = 136
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 446
      Height = 96
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = 'This is Form3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
    end
  end
end
