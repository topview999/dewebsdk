﻿unit unit1;

interface

uses
    dwSGUnit,      //StringGrid控制单元
    Unit2,
    Unit3,
    Unit4,
    //
    dwBase,

    //
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    DateUtils,SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.ComCtrls, Vcl.Imaging.pngimage,
  VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart, VCLTee.Series, Data.DB,
  Data.Win.ADODB, Vcl.DBGrids;

type
  TForm1 = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel_0_Banner: TPanel;
    Panel_Title: TPanel;
    Label_Title: TLabel;
    Image1: TImage;
    Button_Dynamic: TButton;
    Button4: TButton;
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Button_DynamicClick(Sender: TObject);
    procedure PageControl1EndDock(Sender, Target: TObject; X, Y: Integer);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    Form2: TForm2;
    Form3: TForm3;
    Form4: TForm4;

    gsMainDir   : String;
    goConnection    : TADOConnection;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.Button4Click(Sender: TObject);
begin
    dwShowModalPro(self,self.Form4);
end;

procedure TForm1.Button_DynamicClick(Sender: TObject);
begin

    //----------Form1----------------------------------------------------------------------------
    if self.Form3 = nil then begin
        //创建FORM
        self.Form3   := TForm3.Create(self);
        //嵌入到TabSheet中
        self.Form3.Parent  := self.TabSheet3;
        //设置嵌入标识,必须
        self.Form3.HelpKeyword := 'embed';
        self.Form3.ADOQuery2.Connection := goConnection;
        //
        self.Form3.Width        := self.PageControl1.Pages[0].Width;
        self.Form3.Height       := self.PageControl1.Pages[0].Height-10;
        self.Form3.ADOQuery2.SQL.Text  := 'SELECT * FROM dw_member';
        self.Form3.ADOQuery2.Open;
    end;

    //
    self.PageControl1.Pages[2].TabVisible  := True;
    self.PageControl1.ActivePageIndex  := 2;

    //
    DockSite  := True;

end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var
    iW,iH   : Integer;
begin
    dwSetPCMode(self);
    //
    iW  := self.PageControl1.Pages[0].Width;
    iH  := self.PageControl1.Pages[0].Height-10;
    //
    //
    self.Form2.Width        := iW;
    self.Form2.Height       := iH;
    //
    if Self.Form3 <> nil then begin
        self.Form3.Width        := iW;
        self.Form3.Height       := iH;
    end;



end;

procedure TForm1.PageControl1EndDock(Sender, Target: TObject; X, Y: Integer);
begin
    if X = 0 then begin
        PageControl1.Pages[Y].TabVisible    := False;
    end;
end;

end.
