﻿unit Unit3;

interface

uses
    //
    dwBase,

    //
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, Data.DB,
  Data.Win.ADODB, Vcl.DBGrids, Vcl.Buttons;

type
  TForm3 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    ADOQuery1: TADOQuery;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    ADOQuery2: TADOQuery;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure BitBtn1EndDock(Sender, Target: TObject; X, Y: Integer);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure Edit3KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
    Unit1;

{$R *.dfm}

procedure TForm3.BitBtn1EndDock(Sender, Target: TObject; X, Y: Integer);
begin
    dwMessage('Uploaded','success',self);
end;

procedure TForm3.Button1Click(Sender: TObject);
begin
    //dwUpload(TForm1(self.Parent.Owner),'image/gif, image/jpeg','');
    ADOQuery2.SQL.Text  := 'SELECT * FROM dw_member';
    ADOQuery2.Open;
end;

procedure TForm3.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
    if Key=#13 then begin
        dwsetfocus(Edit2);
    end;

end;

procedure TForm3.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
    if Key=#13 then begin
        dwsetfocus(Edit3);
    end;

end;

procedure TForm3.Edit3KeyPress(Sender: TObject; var Key: Char);
begin
    if Key=#13 then begin
        dwsetfocus(Edit1);
    end;

end;

end.
