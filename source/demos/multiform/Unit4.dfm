object Form4: TForm4
  Left = 0
  Top = 100
  BorderStyle = bsNone
  Caption = 'Form4'
  ClientHeight = 281
  ClientWidth = 488
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 482
    Height = 275
    Align = alClient
    Caption = 'Panel1'
    Color = clInfoBk
    ParentBackground = False
    TabOrder = 0
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 480
      Height = 96
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = 'This is Form4'#65281
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      ExplicitWidth = 446
    end
    object Edit1: TEdit
      Left = 40
      Top = 96
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'Edit1'
      OnKeyPress = Edit1KeyPress
    end
    object Edit2: TEdit
      Left = 40
      Top = 128
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'Edit2'
      OnKeyPress = Edit2KeyPress
    end
    object Edit3: TEdit
      Left = 40
      Top = 160
      Width = 121
      Height = 21
      TabOrder = 2
      Text = 'Edit3'
      OnKeyPress = Edit3KeyPress
    end
  end
end
