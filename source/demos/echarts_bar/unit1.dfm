object Form1: TForm1
  Left = 0
  Top = 0
  HelpType = htKeyword
  Margins.Top = 20
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 541
  ClientWidth = 844
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -17
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnMouseDown = FormMouseDown
  PixelsPerInch = 96
  TextHeight = 23
  object Memo1: TMemo
    AlignWithMargins = True
    Left = 3
    Top = 44
    Width = 414
    Height = 414
    HelpType = htKeyword
    HelpKeyword = 'echarts'
    Lines.Strings = (
      '{'
      '  xAxis: {'
      '    type: '#39'category'#39','
      '    data: ['#39'Mon'#39', '#39'Tue'#39', '#39'Wed'#39', '#39'Thu'#39', '#39'Fri'#39', '#39'Sat'#39', '#39'Sun'#39']'
      '  },'
      '  yAxis: {'
      '    type: '#39'value'#39
      '  },'
      '  series: ['
      '    {'
      '      data: [120, 200, 150, 80, 70, 110, 130],'
      '      type: '#39'bar'#39
      '    }'
      '  ]'
      '}')
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 844
    Height = 41
    Align = alTop
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    object Button2: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 117
      Height = 33
      Hint = '{"type":"primary"}'
      Align = alLeft
      Caption = 'Update'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindow
      Font.Height = -17
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = Button2Click
    end
  end
end
