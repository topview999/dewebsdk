﻿unit unit1;

interface

uses
     dwBase,
     //
     Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
     Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Data.DB,
  Data.Win.ADODB;

type
  TForm1 = class(TForm)
    Label_Title: TLabel;
    ScrollBox1: TScrollBox;
    Panel_Main: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel0: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label0: TLabel;
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure ScrollBox1EndDock(Sender, Target: TObject; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
     Form1             : TForm1;


implementation


{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
    iPanel  : Integer;
begin
    //
    for iPanel := 0 to 9 do begin
        with TPanel(FindComponent('Panel'+IntToStr(iPanel))) do begin
            Height  := 94;
            Left    := 3;
            Top     := iPanel * (Height+6) + 3;
            Color   := Rgb(iPanel*25,128,255-iPanel*25);
            Width   := Panel_Main.Width -6;
        end;;
        with TLabel(FindComponent('Label'+IntToStr(iPanel))) do begin
            Caption := 'Panel '+IntToStr(iPanel);
        end;
    end;
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    //设置当前屏幕显示模式为移动应用模式.如果电脑访问，则按414x726（iPhone6/7/8 plus）显示
    dwBase.dwSetMobileMode(self,414,736);
end;

procedure TForm1.ScrollBox1EndDock(Sender, Target: TObject; X, Y: Integer);
var
    iPanel  : Integer;
    iTop    : Integer;
    //
    oPanel  : TPanel;
    oLabel  : TLabel;
begin
    //滚动条事件自动激活OnEndDock事件，其中X为滚动值，Y为方向，1：向下，-1向上

    //
    for iPanel := 0 to 9 do begin
        oPanel  := TPanel(FindComponent('Panel'+IntToStr(iPanel)));
        oLabel  := TLabel(FindComponent('Label'+IntToStr(iPanel)));
        //
        if oPanel.Top  - X < - 200 then begin
            oPanel.Top := oPanel.Top + 1000;
        end else if oPanel.Top  - X > 800 then begin
                oPanel.Top := oPanel.Top - 1000;
        end;
        oLabel.Caption := 'Panel '+IntToStr((oPanel.Top) div 100);
    end;
end;

end.
