﻿unit unit1;

interface

uses
    //
    dwBase,dwSGUnit,

    //
    CloneComponents,
    SynCommons,

    //
    Math, Graphics, DateUtils,
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.Buttons, Data.DB, Data.Win.ADODB, Vcl.DBGrids, Vcl.Mask,
  Vcl.DBCtrls, Vcl.ComCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.MySQL, FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TForm1 = class(TForm)
    Panel_0_Banner: TPanel;
    Label3: TLabel;
    Panel_Title: TPanel;
    Label_Title: TLabel;
    DataSource1: TDataSource;
    Edit_Search: TEdit;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Button_Save: TButton;
    Edit_AName: TEdit;
    CheckBox_Sex: TCheckBox;
    ComboBox_Province: TComboBox;
    Edit_Headship: TEdit;
    DateTimePicker_Birthday: TDateTimePicker;
    Memo_Addr: TMemo;
    FDConnection1: TFDConnection;
    FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink;
    FDQuery1: TFDQuery;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Edit_SearchChange(Sender: TObject);
    procedure Button_SaveClick(Sender: TObject);
    procedure ADOQuery1AfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.ADOQuery1AfterScroll(DataSet: TDataSet);
begin
    //
    Edit_AName.Text                 := ADOQuery1.FieldByName('AName').AsString;
    CheckBox_Sex.Checked            := ADOQuery1.FieldByName('Sex').AsBoolean;
    ComboBox_Province.Text          := ADOQuery1.FieldByName('Province').AsString;
    Edit_Headship.Text              := ADOQuery1.FieldByName('HeadShip').AsString;
    DateTimePicker_Birthday.Date    := ADOQuery1.FieldByName('Birthday').AsDateTime;
    Memo_Addr.Text                  := ADOQuery1.FieldByName('Addr').AsString;

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
    //
    if ADOQuery1.State = dsEdit then begin
        ADOQuery1.Post;
    end;

end;

procedure TForm1.Button_SaveClick(Sender: TObject);
begin
    ADOQuery1.Edit;
    ADOQuery1.FieldByName('AName').AsString     := Edit_AName.Text;
    ADOQuery1.FieldByName('Sex').AsBoolean      := CheckBox_Sex.Checked;
    ADOQuery1.FieldByName('Province').AsString  := ComboBox_Province.Text;
    ADOQuery1.FieldByName('HeadShip').AsString  := Edit_HeadShip.Text;
    ADOQuery1.FieldByName('Birthday').AsDateTime:= DateTimePicker_Birthday.Date;
    ADOQuery1.FieldByName('Addr').AsString      := Memo_Addr.Text;
    ADOQuery1.Post;
    //
    dwMessage('保存成功！','success',self);

end;

procedure TForm1.Edit_SearchChange(Sender: TObject);
begin
    //
    ADOQuery1.Close;
    ADOQuery1.SQL.Text  := 'SELECT * FROM dw_Member WHERE Addr Like ''%'+Edit_Search.text+'%''';
    ADOQuery1.Open;
    //
    ADOQuery1.FieldByName('Birthday').Tag   := 1;

end;

procedure TForm1.FormShow(Sender: TObject);
begin
    //
    ADOQuery1.Close;
    ADOQuery1.SQL.Text  := 'SELECT * FROM dw_Member';
    ADOQuery1.Open;
    //
    //
    ADOQuery1.FieldByName('Birthday').Tag   := 1;
end;

end.
