﻿unit unit1;

interface

uses
    //
    Unit2,

    //
    dwBase,

    //
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.StdCtrls, Vcl.Grids, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Menus,
    Vcl.Buttons, Data.DB, Data.Win.ADODB, Vcl.Samples.Spin;

type
  TForm1 = class(TForm)
    Button1: TButton;
    ADOQuery1: TADOQuery;
    ADOConnection1: TADOConnection;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ADOQuery1AfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    gsMainDir   : String;
    Form2: TForm2;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}



procedure TForm1.ADOQuery1AfterOpen(DataSet: TDataSet);
begin
    Button1.Caption := 'Successs';
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
    Timer1.DesignInfo   := 1;
    Timer1.Tag          := 0;
    dwShowModal(self,Form2);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
    case Timer1.Tag of
        0 : begin;
            //dwMessage('waiting','success',self);
            Timer1.Tag  := Timer1.Tag + 1;
        end ;
        1 : begin
            ADOQuery1.SQL.Text   := 'WAITFOR DELAY ''00:00:05:00'' SELECT 1;' ;
            ADOQUery1.Open;
            Button1.Caption := GetTickCount.ToString;
            Timer1.Tag  := Timer1.Tag + 1;
        end
    else
        Button1.Caption := 'Query Success';
        Timer1.DesignInfo   := 0;
        //关闭窗体
        //dwRunJS('this.'+Form2.Name+'__vis=false;',self);
    end;

end;

end.
