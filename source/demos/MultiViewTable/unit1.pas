﻿unit unit1;

interface

uses
    //
    dwBase,
    dwMultiView,

    //
    CloneComponents,
    SynCommons,

    //
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    DateUtils,SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.ComCtrls, VclTee.TeeGDIPlus, Vcl.Menus,
    VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart, Vcl.Buttons, Data.DB, Data.Win.ADODB,
    Vcl.Imaging.pngimage, Vcl.Samples.Spin;

type
  TForm1 = class(TForm)
    Panel_1_Content: TPanel;
    Panel_0_Banner: TPanel;
    Label_Introduce: TLabel;
    Panel_Title: TPanel;
    Label_Title: TLabel;
    Panel_Record: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel2: TPanel;
    Panel_T_C_Client: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel14: TPanel;
    TrackBar_Page: TTrackBar;
    Panel5: TPanel;
    Panel6: TPanel;
    Label2: TLabel;
    Panel10: TPanel;
    CheckBox2: TCheckBox;
    Panel11: TPanel;
    性别: TLabel;
    Panel12: TPanel;
    Label4: TLabel;
    Panel13: TPanel;
    Panel15: TPanel;
    Label6: TLabel;
    Panel16: TPanel;
    Label7: TLabel;
    Label_Name: TLabel;
    Label_Sex: TLabel;
    ComboBox_HeadShip: TComboBox;
    Label_Province: TLabel;
    Edit_Province: TEdit;
    SpinEdit_Salary: TSpinEdit;
    Label_Addr: TLabel;
    ADOQuery1: TADOQuery;
    Panel1: TPanel;
    CheckBox1: TCheckBox;
    Image_Avatar: TImage;
    Button_ID: TButton;
    Panel_Records: TPanel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.FormShow(Sender: TObject);
begin
    ADOQuery1.SQL.Text  := 'SELECT * FROM dw_Member';
    ADOQuery1.Open;
    //
    dwDataToViews(ADOQuery1,Panel_Record,
        '{"align":0,"items":['
            +'{"field":"AName","type":"string","component":"Label_Name"}'
            +',{"field":"HeadShip","type":"string","format":"","component":"ComboBox_HeadShip"}'
            +',{"field":"Sex","type":"boolean","format":["男","女"],"component":"Label_Sex"}'
            +',{"field":"Province","type":"string","format":"%s省","component":"Edit_Province"}'
            +',{"field":"Addr","type":"string","format":"","component":"Label_Addr"}'
            +',{"field":"Salary","type":"integer","format":"","component":"SpinEdit_Salary"}'
            +',{"field":"ID","type":"integer","format":"工号：%.4d","component":"Button_ID"}'
            +',{"field":"Photo","type":"image","format":"media/images/dwms/u%s.png","component":"Image_Avatar"}'
        +']}');

end;

end.
