﻿unit unit1;

interface

uses
    //
    dwBase,

    //
    CloneComponents,


    //
    Math,
    Graphics,strutils,
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.Buttons,  Vcl.ComCtrls, Vcl.Menus, VclTee.TeeGDIPlus,
  VCLTee.TeEngine, VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart, Data.DB, Vcl.DBGrids, Vcl.WinXCtrls;

type
  TForm1 = class(TForm)
    Panel_Full: TPanel;
    Panel_Inner: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Chart4: TChart;
    Series7: TPieSeries;
    Panel_L: TPanel;
    Panel7: TPanel;
    Chart5: TChart;
    Series8: TAreaSeries;
    Series9: TAreaSeries;
    Panel1: TPanel;
    Panel_R: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel_C: TPanel;
    Chart2: TChart;
    Series5: TBarSeries;
    Series6: TBarSeries;
    ToggleSwitch1: TToggleSwitch;
    ToggleSwitch2: TToggleSwitch;
    ToggleSwitch3: TToggleSwitch;
    ToggleSwitch4: TToggleSwitch;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    ProgressBar1: TProgressBar;
    ProgressBar3: TProgressBar;
    ProgressBar2: TProgressBar;
    ProgressBar4: TProgressBar;
    Panel4: TPanel;
    Label13: TLabel;
    Timer1: TTimer;
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Timer1Timer(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    //刷新以启动动画
    DockSite        := True;
    //清除启动事件，以避免循环执行
    OnMouseDown    := nil;

end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
    iW,iH   : Integer;
    sCode   : String;
begin
    iW      := StrToIntDef(dwGetProp(self,'innerwidth'),0);
    iH      := StrToIntDef(dwGetProp(self,'innerheight'),0);
    //
    if iW<=Width then begin
        sCode   :='document.body.parentNode.style.overflow = "hidden";'
                +'document.getElementById(''app'').style.transformOrigin ="0 0";'
                +'document.getElementById(''app'').style.transform = ''scale('+FloatToStr(iW/Width)+','+FloatToStr(iH/Height)+')'';';
    end else begin
        sCode   :='document.body.parentNode.style.overflow = "hidden";'
                +'document.getElementById(''app'').style.transformOrigin ="50% 0";'
                +'document.getElementById(''app'').style.transform = ''scale('+FloatToStr(iW/Width)+','+FloatToStr(iH/Height)+')'';';
    end;
    dwRunJS(sCode,self);

end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
    I       : Integer;
const
    _SS1    : array[0..9] of String=('一月','二月','三月','四月','五月','六月','七月','八月','九月','十月');
begin
    //
    //dwSetPCMode(Self);

    Randomize;
    Label6.Caption      := Format('%d',[200 ]);   //+Random(200)
    Label10.Caption     := Format('%d',[300 ]);   //+Random(300)
    Label11.Caption     := Format('%d',[400 ]);   //+Random(50)
    Label12.Caption     := Format('%d',[500 ]);    //+Random(200)
    //
    ProgressBar1.Position   := 50+Random(50);
    ProgressBar2.Position   := 50+Random(50);
    ProgressBar3.Position   := 50+Random(50);
    ProgressBar4.Position   := 50+Random(50);
    //
    ToggleSwitch1.State     := TToggleSwitchState(Random(10)>2);
    ToggleSwitch2.State     := TToggleSwitchState(Random(10)>4);
    ToggleSwitch3.State     := TToggleSwitchState(Random(10)>6);
    ToggleSwitch4.State     := TToggleSwitchState(Random(10)>7);
    //

    //
    Randomize;
    Series5.Clear;
    Series6.Clear;
    Series7.Clear;
    Series8.Clear;
    Series9.Clear;
    for I:= 0 to 9 do begin
      Series5.AddY(Random(100));
      Series6.AddY(Random(100));
      Series8.AddY(Random(100),_SS1[I]);
      Series9.AddY(Random(100),_SS1[I]);
      //
      if I<4 then begin
           Series7.AddPie(Random(100),'二程'+IntToStr(I+1)+'部',Random($8FFFFF)*2);
      end;
    end;
end;

end.
