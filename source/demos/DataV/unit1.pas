﻿unit unit1;

interface

uses
     dwBase,
     //
     Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
     Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Data.DB,
  Data.Win.ADODB, Vcl.Menus, Vcl.Grids, Vcl.DBGrids;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Timer1: TTimer;
    Panel2: TPanel;
    StringGrid1: TStringGrid;
    ProgressBar1: TProgressBar;
    Panel3: TPanel;
    Memo1: TMemo;
    Panel4: TPanel;
    StringGrid2: TStringGrid;
    StringGrid3: TStringGrid;
    ProgressBar2: TProgressBar;
    StringGrid4: TStringGrid;
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
        Procedure UpdateDatas;
  end;

var
     Form1             : TForm1;


implementation


{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
    iRow    : Integer;
begin
    with StringGrid1 do begin
        iRow    := 0;
        Cells[0,iRow]   := '北京';  Cells[1,iRow]   := '55';    Cells[2,iRow]   := '#FF5D1D';   Inc(iRow);
        Cells[0,iRow]   := '上海';  Cells[1,iRow]   := '98 ';   Cells[2,iRow]   := '#FFA91F';   Inc(iRow);
        Cells[0,iRow]   := '广州';  Cells[1,iRow]   := '76';    Cells[2,iRow]   := '#9DF90D';   Inc(iRow);
        Cells[0,iRow]   := '深圳';  Cells[1,iRow]   := '83';    Cells[2,iRow]   := '#B283FC';   Inc(iRow);
    end;
    with StringGrid2 do begin
        iRow    := 0;
        Cells[0,iRow]   := '北京';  Cells[1,iRow]   := '55';    Cells[2,iRow]   := '#FF5D1D';   Inc(iRow);
        Cells[0,iRow]   := '上海';  Cells[1,iRow]   := '98 ';   Cells[2,iRow]   := '#FFA91F';   Inc(iRow);
        Cells[0,iRow]   := '广州';  Cells[1,iRow]   := '76';    Cells[2,iRow]   := '#9DF90D';   Inc(iRow);
        Cells[0,iRow]   := '深圳';  Cells[1,iRow]   := '83';    Cells[2,iRow]   := '#B283FC';   Inc(iRow);
    end;
    with StringGrid3 do begin
        iRow    := 0;
        Cells[0,iRow]   := '微软';  Cells[1,iRow]   := '98';    Cells[2,iRow]   := 'media/images/datav/1st.png';   Inc(iRow);
        Cells[0,iRow]   := '谷歌';  Cells[1,iRow]   := '96 ';   Cells[2,iRow]   := 'media/images/datav/2st.png';   Inc(iRow);
        Cells[0,iRow]   := '三星';  Cells[1,iRow]   := '76';    Cells[2,iRow]   := 'media/images/datav/3st.png';   Inc(iRow);
        Cells[0,iRow]   := '华为';  Cells[1,iRow]   := '83';    Cells[2,iRow]   := 'media/images/datav/4st.png';   Inc(iRow);
        Cells[0,iRow]   := '迈华';  Cells[1,iRow]   := '72';    Cells[2,iRow]   := 'media/images/datav/5st.png';   Inc(iRow);
        Cells[0,iRow]   := '小米';  Cells[1,iRow]   := '66';    Cells[2,iRow]   := 'media/images/datav/6st.png';   Inc(iRow);
        Cells[0,iRow]   := '搜狐';  Cells[1,iRow]   := '32';    Cells[2,iRow]   := 'media/images/datav/7st.png';   Inc(iRow);
    end;
    with StringGrid4 do begin
        iRow    := 0;
        Cells[0,iRow]   := '公司';  Cells[1,iRow]   := '市值';    Cells[2,iRow]   := '网址';   Inc(iRow);
        Cells[0,iRow]   := '微软';  Cells[1,iRow]   := '198';   Cells[2,iRow]   := 'microsoft';   Inc(iRow);
        Cells[0,iRow]   := '谷歌';  Cells[1,iRow]   := '196 ';  Cells[2,iRow]   := 'google';   Inc(iRow);
        Cells[0,iRow]   := '三星';  Cells[1,iRow]   := '16';    Cells[2,iRow]   := 'sansung';   Inc(iRow);
        Cells[0,iRow]   := '华为';  Cells[1,iRow]   := '83';    Cells[2,iRow]   := 'huawei';   Inc(iRow);
        Cells[0,iRow]   := '迈华';  Cells[1,iRow]   := '72';    Cells[2,iRow]   := 'maxxua';   Inc(iRow);
        Cells[0,iRow]   := '小米';  Cells[1,iRow]   := '66';    Cells[2,iRow]   := 'xiaomi';   Inc(iRow);
        Cells[0,iRow]   := '搜狐';  Cells[1,iRow]   := '32';    Cells[2,iRow]   := 'sohu';   Inc(iRow);
        Cells[0,iRow]   := '新浪';  Cells[1,iRow]   := '26';    Cells[2,iRow]   := 'sina';   Inc(iRow);
        Cells[0,iRow]   := '字节';  Cells[1,iRow]   := '22';    Cells[2,iRow]   := 'byte';   Inc(iRow);
        Cells[0,iRow]   := '网易';  Cells[1,iRow]   := '18';    Cells[2,iRow]   := '163.com';   Inc(iRow);
    end;
end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    StringGrid1.ParentCustomHint    := False;
    Docksite    := True;
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    //设置当前屏幕显示模式为移动应用模式.如果电脑访问，则按414x726（iPhone6/7/8 plus）显示
    //dwBase.dwSetMobileMode(self,414,736);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
    UpdateDatas;
end;

procedure TForm1.UpdateDatas;
var
    iRow    : Integer;
begin
    Tag := Tag + 1;
    Randomize;
    if Tag mod 4 = 1 then begin
        with StringGrid1 do begin
            iRow    := 0;
            Cells[0,iRow]   := '北京';  Cells[1,iRow]   := IntToStr(50+Random(50));     Cells[2,iRow]   := '#FF5D1D';   Inc(iRow);
            Cells[0,iRow]   := '上海';  Cells[1,iRow]   := IntToStr(50+Random(50));     Cells[2,iRow]   := '#FFA91F';   Inc(iRow);
            Cells[0,iRow]   := '广州';  Cells[1,iRow]   := IntToStr(50+Random(50));     Cells[2,iRow]   := '#9DF90D';   Inc(iRow);
            Cells[0,iRow]   := '深圳';  Cells[1,iRow]   := IntToStr(50+Random(50));     Cells[2,iRow]   := '#B283FC';   Inc(iRow);
        end;
    end;
    with StringGrid2 do begin
        iRow    := 0;
        Cells[0,iRow]   := '北京';  Cells[1,iRow]   := IntToStr(50+Random(50));     Cells[2,iRow]   := '#FF5D1D';   Inc(iRow);
        Cells[0,iRow]   := '上海';  Cells[1,iRow]   := IntToStr(50+Random(50));     Cells[2,iRow]   := '#FFA91F';   Inc(iRow);
        Cells[0,iRow]   := '广州';  Cells[1,iRow]   := IntToStr(50+Random(50));     Cells[2,iRow]   := '#9DF90D';   Inc(iRow);
        Cells[0,iRow]   := '深圳';  Cells[1,iRow]   := IntToStr(50+Random(50));     Cells[2,iRow]   := '#B283FC';   Inc(iRow);
    end;
    //
    ProgressBar1.Position   := 30+Random(40);
    ProgressBar2.Position   := 40+Random(60);
end;

end.
