object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 800
  ClientWidth = 1100
  Color = 2954240
  TransparentColor = True
  TransparentColorValue = 2954240
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnMouseDown = FormMouseDown
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 20
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 480
    Height = 800
    HelpKeyword = 'dvbox'
    HelpContext = 11
    Align = alLeft
    Caption = 'DeWeb'
    Color = clNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = #24494#36719#38597#40657
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 478
      Height = 296
      Align = alTop
      Caption = 'Panel2'
      ParentColor = True
      TabOrder = 0
      object StringGrid1: TStringGrid
        AlignWithMargins = True
        Left = 1
        Top = 61
        Width = 342
        Height = 231
        HelpType = htKeyword
        HelpKeyword = 'dvring'
        Margins.Left = 0
        Margins.Top = 60
        Margins.Right = 0
        Align = alClient
        BevelOuter = bvNone
        BorderStyle = bsNone
        ColCount = 2
        TabOrder = 0
        RowHeights = (
          24
          24
          24
          24
          24)
      end
      object ProgressBar1: TProgressBar
        AlignWithMargins = True
        Left = 343
        Top = 81
        Width = 104
        Height = 211
        HelpType = htKeyword
        HelpKeyword = 'dvwater'
        Margins.Left = 0
        Margins.Top = 80
        Margins.Right = 30
        Align = alRight
        TabOrder = 1
      end
    end
    object Panel4: TPanel
      AlignWithMargins = True
      Left = 21
      Top = 307
      Width = 438
      Height = 472
      HelpKeyword = 'dvbox'
      HelpContext = 10
      Margins.Left = 20
      Margins.Top = 10
      Margins.Right = 20
      Margins.Bottom = 20
      Align = alClient
      Color = clNone
      ParentBackground = False
      TabOrder = 1
      object StringGrid2: TStringGrid
        AlignWithMargins = True
        Left = 31
        Top = 11
        Width = 376
        Height = 171
        HelpType = htKeyword
        HelpKeyword = 'dvcapsule'
        Margins.Left = 30
        Margins.Top = 10
        Margins.Right = 30
        Align = alTop
        BevelOuter = bvNone
        BorderStyle = bsNone
        ColCount = 2
        TabOrder = 0
      end
      object StringGrid4: TStringGrid
        AlignWithMargins = True
        Left = 31
        Top = 195
        Width = 376
        Height = 266
        HelpType = htKeyword
        HelpKeyword = 'dvscroll'
        Margins.Left = 30
        Margins.Top = 10
        Margins.Right = 30
        Margins.Bottom = 10
        Align = alClient
        BevelOuter = bvNone
        BorderStyle = bsNone
        ColCount = 3
        RowCount = 10
        TabOrder = 1
      end
    end
  end
  object Panel3: TPanel
    AlignWithMargins = True
    Left = 483
    Top = 3
    Width = 614
    Height = 794
    HelpKeyword = 'dvbox'
    HelpContext = 11
    Align = alClient
    Caption = #39134#32447#22270
    Color = clNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = #24494#36719#38597#40657
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object Memo1: TMemo
      AlignWithMargins = True
      Left = 21
      Top = 61
      Width = 572
      Height = 401
      HelpType = htKeyword
      HelpKeyword = 'dvflyline'
      Margins.Left = 20
      Margins.Top = 60
      Margins.Right = 20
      Margins.Bottom = 20
      Align = alClient
      Lines.Strings = (
        '{'
        '  points: ['
        '    {'
        '      name: '#39#35140#38451#39','
        '      coordinate: [0.48, 0.35],'
        '      halo: {'
        '        show: true,'
        '      },'
        '      icon: {'
        '        src: '#39'media/images/datav/mapCenterPoint.png'#39','
        '        width: 30,'
        '        height: 30'
        '      },'
        '      text: {'
        '        show: false'
        '      }'
        '    },'
        '    {'
        '      name: '#39#26032#20065#39','
        '      coordinate: [0.52, 0.23]'
        '    },'
        '    {'
        '      name: '#39#28966#20316#39','
        '      coordinate: [0.43, 0.29]'
        '    },'
        '    {'
        '      name: '#39#24320#23553#39','
        '      coordinate: [0.59, 0.35]'
        '    },'
        '    {'
        '      name: '#39#35768#26124#39','
        '      coordinate: [0.53, 0.47]'
        '    },'
        '    {'
        '      name: '#39#24179#39030#23665#39','
        '      coordinate: [0.45, 0.54]'
        '    },'
        '    {'
        '      name: '#39#27931#38451#39','
        '      coordinate: [0.36, 0.38]'
        '    },'
        '    {'
        '      name: '#39#21608#21475#39','
        '      coordinate: [0.62, 0.55],'
        '      halo: {'
        '        show: true,'
        '        color: '#39'#8378ea'#39
        '      }'
        '    },'
        '    {'
        '      name: '#39#28463#27827#39','
        '      coordinate: [0.56, 0.56]'
        '    },'
        '    {'
        '      name: '#39#21335#38451#39','
        '      coordinate: [0.37, 0.66],'
        '      halo: {'
        '        show: true,'
        '        color: '#39'#37a2da'#39
        '      }'
        '    },'
        '    {'
        '      name: '#39#20449#38451#39','
        '      coordinate: [0.55, 0.81]'
        '    },'
        '    {'
        '      name: '#39#39547#39532#24215#39','
        '      coordinate: [0.55, 0.67]'
        '    },'
        '    {'
        '      name: '#39#27982#28304#39','
        '      coordinate: [0.37, 0.29]'
        '    },'
        '    {'
        '      name: '#39#19977#38376#23777#39','
        '      coordinate: [0.20, 0.36]'
        '    },'
        '    {'
        '      name: '#39#21830#19992#39','
        '      coordinate: [0.76, 0.41]'
        '    },'
        '    {'
        '      name: '#39#40548#22721#39','
        '      coordinate: [0.59, 0.18]'
        '    },'
        '    {'
        '      name: '#39#28654#38451#39','
        '      coordinate: [0.68, 0.17]'
        '    },'
        '    {'
        '      name: '#39#23433#38451#39','
        '      coordinate: [0.59, 0.10]'
        '    }'
        '  ],'
        '  lines: ['
        '    {'
        '      source: '#39#26032#20065#39','
        '      target: '#39#35140#38451#39
        '    },'
        '    {'
        '      source: '#39#28966#20316#39','
        '      target: '#39#35140#38451#39
        '    },'
        '    {'
        '      source: '#39#24320#23553#39','
        '      target: '#39#35140#38451#39
        '    },'
        '    {'
        '      source: '#39#21608#21475#39','
        '      target: '#39#35140#38451#39','
        '      color: '#39'#fb7293'#39','
        '      width: 2'
        '    },'
        '    {'
        '      source: '#39#21335#38451#39','
        '      target: '#39#35140#38451#39','
        '      color: '#39'#fb7293'#39','
        '      width: 2'
        '    },'
        '    {'
        '      source: '#39#27982#28304#39','
        '      target: '#39#35140#38451#39
        '    },'
        '    {'
        '      source: '#39#19977#38376#23777#39','
        '      target: '#39#35140#38451#39
        '    },'
        '    {'
        '      source: '#39#21830#19992#39','
        '      target: '#39#35140#38451#39
        '    },'
        '    {'
        '      source: '#39#40548#22721#39','
        '      target: '#39#35140#38451#39
        '    },'
        '    {'
        '      source: '#39#28654#38451#39','
        '      target: '#39#35140#38451#39
        '    },'
        '    {'
        '      source: '#39#23433#38451#39','
        '      target: '#39#35140#38451#39
        '    },'
        '    {'
        '      source: '#39#35768#26124#39','
        '      target: '#39#21335#38451#39','
        '      color: '#39'#37a2da'#39
        '    },'
        '    {'
        '      source: '#39#24179#39030#23665#39','
        '      target: '#39#21335#38451#39','
        '      color: '#39'#37a2da'#39
        '    },'
        '    {'
        '      source: '#39#27931#38451#39','
        '      target: '#39#21335#38451#39','
        '      color: '#39'#37a2da'#39
        '    },'
        '    {'
        '      source: '#39#39547#39532#24215#39','
        '      target: '#39#21608#21475#39','
        '      color: '#39'#8378ea'#39
        '    },'
        '    {'
        '      source: '#39#20449#38451#39','
        '      target: '#39#21608#21475#39','
        '      color: '#39'#8378ea'#39
        '    },'
        '    {'
        '      source: '#39#28463#27827#39','
        '      target: '#39#21608#21475#39','
        '      color: '#39'#8378ea'#39
        '    }'
        '  ],'
        '  icon: {'
        '    show: true,'
        '    src: '#39'media/images/datav/mapPoint.png'#39
        '  },'
        '  text: {'
        '    show: true,'
        '  },'
        '  k: 0.5,'
        '  bgImgSrc: '#39'media/images/datav/map.jpg'#39
        '}')
      TabOrder = 0
    end
    object StringGrid3: TStringGrid
      AlignWithMargins = True
      Left = 51
      Top = 583
      Width = 512
      Height = 200
      HelpType = htKeyword
      HelpKeyword = 'dvconical'
      Margins.Left = 50
      Margins.Top = 10
      Margins.Right = 50
      Margins.Bottom = 10
      Align = alBottom
      BevelOuter = bvNone
      BorderStyle = bsNone
      ColCount = 3
      RowCount = 7
      TabOrder = 1
    end
    object ProgressBar2: TProgressBar
      AlignWithMargins = True
      Left = 31
      Top = 492
      Width = 552
      Height = 78
      HelpType = htKeyword
      HelpKeyword = 'dvpond'
      Margins.Left = 30
      Margins.Top = 10
      Margins.Right = 30
      Align = alBottom
      Position = 75
      TabOrder = 2
    end
  end
  object Timer1: TTimer
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 160
    Top = 208
  end
end
