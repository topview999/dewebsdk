﻿unit unit1;

interface

uses
    //
    dwBase,
    SynCommons,


    //
    Math,DateUtils,
    Graphics,strutils,
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.Buttons,  Vcl.ComCtrls, Vcl.Samples.Calendar,
  Vcl.Samples.Spin;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Calendar1: TCalendar;
    Panel2: TPanel;
    Label1: TLabel;
    Panel3: TPanel;
    Label2: TLabel;
    Panel4: TPanel;
    Label3: TLabel;
    Edit_Caption: TEdit;
    DateTimePicker_Date: TDateTimePicker;
    Panel5: TPanel;
    Label4: TLabel;
    SpinEdit_Last: TSpinEdit;
    Panel6: TPanel;
    Label5: TLabel;
    Edit_Color: TEdit;
    Button_Add: TButton;
    Button_Delete: TButton;
    ComboBox_Schedule: TComboBox;
    procedure Button_AddClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button_DeleteClick(Sender: TObject);
  private
    { Private declarations }
  public
    gsMainDir   : String;
        procedure UpdateSchedules;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

function dwAddSchedule(ACtrl:TControl;ACaption:String;ADate:TDate;ALast:Word;AColor:String):Integer;
var
    sHint   : string;
    joHint  : Variant;
    joNew   : Variant;
    joTmp   : Variant;
    iSc     : Integer;
    iTmp    : Integer;
    dtSc    : TDate;
    iY,iM   : Word;
    iD      : Word;
    bFound  : Boolean;
begin
    //根据Hint生成JSON
    sHint     := ACtrl.Hint;
    joHint    := _json('{}');
    if dwStrIsJson(sHint) then begin
        joHint    := _json(sHint);
    end;

    //检查是否存在
    if not joHint.Exists('schedule') then begin
        joHint.schedule := _json('[]');
    end;

    //
    bFound  := False;

    for iSc := 0 to joHint.schedule._Count-1 do begin
        iY  := joHint.schedule._(iSc).year;
        iM  := joHint.schedule._(iSc).month;
        iD  := joHint.schedule._(iSc).day;
        //
        dtSc    := EncodeDate(iY,iM,iD);
        if ADate < dtSc then begin
            bFound  := True;
            //joHint.schedule.Add(joNew,iSc+1);

            //用一个笨办法来插入记录
            joTmp   := _json('[]');

            //先添加原日程组信息中前面的
            for iTmp := 0 to iSc-1 do begin
                joTmp.Add(joHint.schedule._(iTmp));
            end;

            //添加新加的
            joNew   := _json('{}');
            joNew.caption   := ACaption;
            DecodeDate(ADate,iY,iM,iD);
            joNew.year  := iY;
            joNew.month := iM;
            joNew.day   := iD;
            joNew.last  := ALast;
            joNew.color := AColor;
            joTmp.Add(joNew);

            //添加原日程组信息中后面的
            for iTmp := iSc to joHint.schedule._Count-1 do begin
                joTmp.Add(joHint.schedule._(iTmp));
            end;

            //还原
            joHint.schedule := joTmp;

            //
            break;

        end;
    end;

    //
    if not bFound then begin
        joNew   := _json('{}');
        joNew.caption   := ACaption;
        DecodeDate(ADate,iY,iM,iD);
        joNew.year  := iY;
        joNew.month := iM;
        joNew.day   := iD;
        joNew.last  := ALast;
        joNew.color := AColor;
        //
        joHint.schedule.Add(joNew);
    end;
    //
    ACtrl.Hint  := joHint;
    //
    Result  := 0;

end;
procedure TForm1.Button_AddClick(Sender: TObject);
begin
    dwAddSchedule(
        Calendar1,                  //日程表控件
        Edit_Caption.Text,          //标题
        DateTimePicker_Date.Date,   //开始日期
        SpinEdit_Last.Value,        //持续天数
        Edit_Color.Text             //显示颜色
    );
    //
    UpdateSchedules;
end;

procedure TForm1.Button_DeleteClick(Sender: TObject);
var
    sHint   : string;
    joHint  : Variant;
    iSc     : Integer;
    iOld    : Integer;
begin
    if ComboBox_Schedule.ItemIndex>=0 then begin
        //根据Hint生成JSON
        sHint     := Calendar1.Hint;
        joHint    := _json('{}');
        if dwStrIsJson(sHint) then begin
            joHint    := _json(sHint);
        end;

        //检查是否存在
        if not joHint.Exists('schedule') then begin
            joHint.schedule := _json('[]');
        end;

        //
        if joHint.schedule._Count>ComboBox_Schedule.ItemIndex then begin
            joHint.schedule.Delete(ComboBox_Schedule.ItemIndex);
        end;
        //
        Calendar1.Hint  := joHint;
    end;
    UpdateSchedules;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
    UpdateSchedules;
end;

procedure TForm1.UpdateSchedules;
var
    sHint   : string;
    joHint  : Variant;
    iSc     : Integer;
    iOld    : Integer;
begin
    //根据Hint生成JSON
    sHint     := Calendar1.Hint;
    joHint    := _json('{}');
    if dwStrIsJson(sHint) then begin
        joHint    := _json(sHint);
    end;

    //检查是否存在
    if not joHint.Exists('schedule') then begin
        joHint.schedule := _json('[]');
    end;

    //
    iOld    := ComboBox_Schedule.ItemIndex;
    ComboBox_Schedule.Items.Clear;
    for iSc := 0 to joHint.schedule._Count-1 do begin
        ComboBox_Schedule.Items.Add('['+IntToStr(iSc)+'] '+joHint.schedule._(iSc).caption);
    end;
    if (iOld >= 0) and (iOld < ComboBox_Schedule.Items.Count) then begin
        ComboBox_Schedule.ItemIndex := iOld;
    end else begin
        ComboBox_Schedule.ItemIndex := 0;
    end
end;

end.
