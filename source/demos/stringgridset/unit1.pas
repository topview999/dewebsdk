﻿unit unit1;

interface

uses
     //
     dwBase,
     dwSGUnit,      //StringGrid控制单元

     
     //

     //
     Math,
     ComObj,
     Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, jpeg, ExtCtrls,  Vcl.Grids,
     Vcl.Imaging.pngimage;

type
  TForm1 = class(TForm)
    Label_Demo: TLabel;
    Panel_Logo: TPanel;
    Image1: TImage;
    Label6: TLabel;
    Panel_All: TPanel;
    Panel_StringGrid: TPanel;
    StringGrid1: TStringGrid;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button1: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button11Click(Sender: TObject);
  private
  public
    gsMainDir : string;

  end;

var
     Form1     : TForm1;


implementation


{$R *.dfm}




procedure TForm1.Button10Click(Sender: TObject);
begin
     dwSGSetRow(StringGrid1,0,clNavy,RGB(100,100,100));

end;

procedure TForm1.Button11Click(Sender: TObject);
var
    iRow    : Integer;
begin
  for iRow:=1 to StringGrid1.RowCount-1 do
    if iRow mod 2=0 then
      dwSGSetRow(StringGrid1,iRow,clInfoBk,clRed)
    else
      dwSGSetRow(StringGrid1,iRow,clWindow,clBlue);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
     StringGrid1.ColWidths[1] := StringGrid1.ColWidths[1] + 5;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
     dwSGSetCellStyle(StringGrid1,0,0,clGreen,'宋体',20,True,False,clWhite);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
     dwSGSetCellStyle(StringGrid1,4,1,clAqua,'隶书',20,False,False,clBlue);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
     dwSGSetRow(StringGrid1,2,clGray,RGB(100,100,100));
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
     dwSGSetCol(StringGrid1,2,clAqua,clBlue);
     //dwSGSetCol(StringGrid1,0,clGray,clBlue);

end;

procedure TForm1.Button6Click(Sender: TObject);
begin
     if StringGrid1.ColWidths[0] < 0 then begin
          StringGrid1.ColWidths[0] := 120;
     end else begin
          StringGrid1.ColWidths[0] := -1;
     end;
end;

procedure TForm1.Button7Click(Sender: TObject);
var
    i,j     : integer;
    xlapp   : variant;
    sheet   : variant;
begin
    try
        xlapp   := createoleobject('excel.application');
    except
        dwMessage('数据传输出错,请确认Microsoft Excel是否安装!','error',self);
        exit;
    end;

    try

        xlapp.WorkBooks.Add;

        //4) 打开已存在的工作簿：
        //xlapp.WorkBooks.Open( 'C:\Excel\Demo.xls' );

        //5) 设置第2个工作表为活动工作表：
        xlapp.WorkSheets[1].Activate;
        //或
        //xlapp.WorksSheets[ 'Sheet2' ].Activate;
        for i:=0 to stringgrid1.rowcount-1 do begin
            for j:=0 to stringgrid1.colcount-1 do begin
                xlapp.Cells[j+1,i+1]:= stringgrid1.cells[i,j];
            end;
        end;
        xlapp.Cells[j+1,1]:= '生成日期:'+FormatDateTime('YYYY-MM-DD hh:mm:ss',Now);
        xlapp.ActiveWorkBook.savecopyas(gsMainDir+'download\sg.xls');
        xlapp.ActiveWorkBook.Saved := True;
        dwMessage('保存成功!','success',self);
    finally
        xlapp.quit;
        xlapp := Unassigned;

    end;
end;


procedure TForm1.Button8Click(Sender: TObject);
begin
    //
    dwOpenUrl(self,'/download/sg.xls','_blank');
end;

procedure TForm1.Button9Click(Sender: TObject);
begin
     dwSGSetCol(StringGrid1,0,clBlack,clGreen);
     //dwSGSetCol(StringGrid1,0,clGray,clBlue);

end;

procedure TForm1.FormCreate(Sender: TObject);
var
    iR,iC   : Integer;
begin

    //
    Top  := 0;
    //
    with StringGrid1 do begin
        Cells[0,0]   := '日期';
        Cells[1,0]   := '姓名';
        Cells[2,0]   := '省份';
        Cells[3,0]   := '城市';
        Cells[4,0]   := '详细地址';
        //
        Cells[0,1]   := '2000-01-01';
        Cells[1,1]   := '张云政';
        Cells[2,1]   := '湖北';
        Cells[3,1]   := '襄阳';
        Cells[4,1]   := '电力局办公室01001号';
        //
        Cells[0,2]   := '2001-07-01';
        Cells[1,2]   := '李景学';
        Cells[2,2]   := '北京';
        Cells[3,2]   := '朝阳区';
        Cells[4,2]   := '烟草公司驻外楼2901室';
        //
        Cells[0,3]   := '2002-12-01';
        Cells[1,3]   := '周子琴';
        Cells[2,3]   := '上海';
        Cells[3,3]   := '淞沪区';
        Cells[4,3]   := '外滩管理事务局管理处';
        //
        Cells[0,4]   := '2006-03-01';
        Cells[1,4]   := '谢玲芳';
        Cells[2,4]   := '辽宁';
        Cells[3,4]   := '沈阳';
        Cells[4,4]   := '民航管理中心监察室9901';
        //
        Cells[0,5]   := '2007-03-01';
        Cells[1,5]   := '张曼玉';
        Cells[2,5]   := '香港';
        Cells[3,5]   := '九龙';
        Cells[4,5]   := '无线集团';
        //
        Cells[0,6]   := '2008-03-01';
        Cells[1,6]   := '刘德华';
        Cells[2,6]   := '甘肃';
        Cells[3,6]   := '兰州';
        Cells[4,6]   := '房地产公司驻外楼1212室';
        //
        Cells[0,7]   := '2009-03-01';
        Cells[1,7]   := '梁朝伟';
        Cells[2,7]   := '山东';
        Cells[3,7]   := '济南';
        Cells[4,7]   := '数据中心';
        //
        Cells[0,8]   := '2010-03-01';
        Cells[1,8]   := '张益';
        Cells[2,8]   := '辽宁';
        Cells[3,8]   := '大连';
        Cells[4,8]   := '金融管理大队';
        //
        Cells[0,9]   := '2011-03-01';
        Cells[1,9]   := '贾玲';
        Cells[2,9]   := '湖北';
        Cells[3,9]   := '宜城';
        Cells[4,9]   := '综合演艺管理中心';
        //
        Cells[0,10]   := '2001-07-01';
        Cells[1,10]   := '李景学';
        Cells[2,10]   := '北京';
        Cells[3,10]   := '朝阳区';
        Cells[4,10]   := '烟草公司驻外楼2901室';
        //
        Cells[0,11]   := '2000-01-01';
        Cells[1,11]   := '张云政';
        Cells[2,11]   := '湖北';
        Cells[3,11]   := '襄阳';
        Cells[4,11]   := '电力局办公室01001号';
        //
        Cells[0,12]   := '2001-07-01';
        Cells[1,12]   := '李景学';
        Cells[2,12]   := '北京';
        Cells[3,12]   := '朝阳区';
        Cells[4,12]   := '烟草公司驻外楼2901室';
        //
        Cells[0,13]   := '2002-12-01';
        Cells[1,13]   := '周子琴';
        Cells[2,13]   := '上海';
        Cells[3,13]   := '淞沪区';
        Cells[4,13]   := '外滩管理事务局管理处';
        //
        Cells[0,14]   := '2006-03-01';
        Cells[1,14]   := '谢玲芳';
        Cells[2,14]   := '辽宁';
        Cells[3,14]   := '沈阳';
        Cells[4,14]   := '民航管理中心监察室9901';
        //
        Cells[0,15]   := '2007-03-01';
        Cells[1,15]   := '张曼玉';
        Cells[2,15]   := '香港';
        Cells[3,15]   := '九龙';
        Cells[4,15]   := '无线集团';
        //
        Cells[0,16]   := '2008-03-01';
        Cells[1,16]   := '刘德华';
        Cells[2,16]   := '甘肃';
        Cells[3,16]   := '兰州';
        Cells[4,16]   := '房地产公司驻外楼1212室';
        //
        Cells[0,17]   := '2009-03-01';
        Cells[1,17]   := '梁朝伟';
        Cells[2,17]   := '山东';
        Cells[3,17]   := '济南';
        Cells[4,17]   := '数据中心';
        //
        Cells[0,18]   := '2010-03-01';
        Cells[1,18]   := '张益';
        Cells[2,18]   := '辽宁';
        Cells[3,18]   := '大连';
        Cells[4,18]   := '金融管理大队';
        //
        Cells[0,19]   := '2011-03-01';
        Cells[1,19]   := '贾玲';
        Cells[2,19]   := '湖北';
        Cells[3,19]   := '宜城';
        Cells[4,19]   := '综合演艺管理中心';
        //
        ColWidths[0]   := -1;
        ColWidths[1]   := 120;
        ColWidths[2]   := 200;
        ColWidths[3]   := 200;
        ColWidths[4]   := 200;
    end;


end;



procedure TForm1.FormShow(Sender: TObject);
begin
    dwMessage('OnShow message','success',self);
    //dwOpenUrl(self,'/hello','_blank');
end;

end.
