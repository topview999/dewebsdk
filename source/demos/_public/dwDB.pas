﻿unit dwDB;

{
DeWeb DataBase Unit
说明： 主要用于DeWeb的数据库操作
}

interface

uses
    ADODB,DB,
    Math,
    Types,
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Grids;



//根据表名，生成多字段查询的WHERE字符串
function dwGetWhere(
        AQuery      : TADOQuery;
        ATable      : string;          //表名
        AFields     : string;           //字段列表  = '*'或'Name,Age,job,title'
        AKeyword    : String
        ):string;


//根据表名、字段名、条件，排序，拟读取的起始记录位置，拟读取的记录数自动读取数据，更新自定义显示和分页
//AFileds = '*'或'Name,Age,job,title'
//AWhere = 'WHERE id>10'
//AOrder = 'ORDER BY name DESC'
//注意:必须有id自增字段
procedure dwGetData(
        AQuery:TADOQuery;       //对应的ADOQuery控件
        ATable:string;          //表名
        AFields:string;         //字段列表  = '*'或'Name,Age,job,title'
        AWhere:string;          //WHERE条件,例: 'WHERE id>10'
        AOrder:String;          //AOrder = 'ORDER BY name DESC'
        AFirst:Integer;         //拟读取的记录位置,从1开始
        ACount:Integer          //拟读取的记录数
        );



implementation

//根据表名、字段名、条件，排序，拟读取的起始记录位置，拟读取的记录数自动读取数据，更新自定义显示和分页
//AFileds = '*'或'Name,Age,job,title'
//AWhere = 'WHERE id>10'
//AOrder = 'ORDER BY name DESC'
//注意:必须有id自增字段
procedure dwGetData(
        AQuery:TADOQuery;       //对应的ADOQuery控件
        ATable:string;          //表名
        AFields:string;         //字段列表  = '*'或'Name,Age,job,title'
        AWhere:string;          //WHERE条件,例: 'WHERE id>10'
        AOrder:String;          //AOrder = 'ORDER BY name DESC'
        AFirst:Integer;         //拟读取的记录位置,从1开始
        ACount:Integer          //每页显示的记录数
        );
var
    S0      : String;   //用于生成拟排除的ID查询
begin
    AQuery.Close;
    if AFirst <= 1 then begin
        AQuery.SQL.Text   := 'SELECT TOP '+ACount.ToString+' '+ AFields+' FROM '+ATable+' '+AWhere+' '+AOrder;
    end else begin
        //先取得AFirst记录位置以前的记录的查询语句
        S0 := 'SELECT TOP '+IntToStr(AFirst-1)+' id FROM '+ATable+' '+AWhere+' '+AOrder;
        if Trim(AWhere) = '' then begin
            AQuery.SQL.Text   := 'SELECT TOP '+IntToStr(ACount)+' '+AFields+' FROM '+ATable+' WHERE (id NOT IN ('+S0+')) '+AOrder;
        end else begin
            AQuery.SQL.Text   := 'SELECT TOP '+IntToStr(ACount)+' '+AFields+' FROM '+ATable+' '+AWhere+' AND (id NOT IN ('+S0+')) '+AOrder;
        end;
    end;
    AQuery.Open;

end;



function dwGetWhere(
        AQuery      : TADOQuery;
        ATable      : string;          //表名
        AFields     : string;          //字段列表  = '*'或'Name,Age,job,title'
        AKeyword    : String
        ):string;
var
    SS      : TStringDynArray;
    iPos    : Integer;
    iKey    : Integer;
    iField  : Integer;
begin
    if Trim(AKeyword)='' then begin
        Result  := ' WHERE (1=1) ';
    end else begin
        //拆分出多个关键字。 如查询 ”delphi 控件开发“
        AKeyword    := Trim(AKeyword);
        while AKeyword<>'' do begin
            iPos := Pos(' ',AKeyword);
            if iPos>0 then begin
                SetLength(SS,Length(SS)+1);
                SS[High(SS)]    := Trim(Copy(AKeyword,1,iPos-1));
                //
                Delete(AKeyword,1,iPos);
                AKeyword    := Trim(AKeyword);
            end else begin
                SetLength(SS,Length(SS)+1);
                SS[High(SS)]    := AKeyword;
                //
                break;
            end;
        end;

        //得到字段名
        AQuery.Close;
        AQuery.SQL.Text := 'SELECT '+AFields+' FROM '+ATable+' WHERE (1=2)';
        AQuery.Open;
        Result  := 'WHERE ';
        for iKey := 0 to High(SS) do begin
            Result  := Result +'(';
            for iField := 0 to AQuery.FieldCount-1 do begin
                //不查询iD字段
                if lowerCase(AQuery.Fields[iField].FieldName)='id' then begin
                    Continue;
                end;
                //
                Result  := Result + AQuery.Fields[iField].FieldName +' like ''%'+SS[iKey]+'%'' OR '
            end;
            Delete(Result,Length(Result)-3,4);
            Result  := Result +') AND ';
        end;
        Delete(Result,Length(Result)-3,4);
    end;

end;


end.
