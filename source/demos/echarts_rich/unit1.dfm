object Form1: TForm1
  Left = 0
  Top = 0
  HelpType = htKeyword
  Margins.Top = 20
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 541
  ClientWidth = 1000
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -17
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnMouseDown = FormMouseDown
  PixelsPerInch = 96
  TextHeight = 23
  object Memo1: TMemo
    AlignWithMargins = True
    Left = 3
    Top = 44
    Width = 994
    Height = 494
    HelpType = htKeyword
    HelpKeyword = 'echarts'
    Align = alClient
    Lines.Strings = (
      '{'
      '  tooltip: {'
      '    trigger: '#39'item'#39','
      '    formatter: '#39'{a} <br/>{b}: {c} ({d}%)'#39
      '  },'
      '  legend: {'
      '    data: ['
      '      '#39'Direct'#39','
      '      '#39'Marketing'#39','
      '      '#39'Search Engine'#39','
      '      '#39'Email'#39','
      '      '#39'Union Ads'#39','
      '      '#39'Video Ads'#39','
      '      '#39'Baidu'#39','
      '      '#39'Google'#39','
      '      '#39'Bing'#39','
      '      '#39'Others'#39
      '    ]'
      '  },'
      '  series: ['
      '    {'
      '      name: '#39'Access From'#39','
      '      type: '#39'pie'#39','
      '      selectedMode: '#39'single'#39','
      '      radius: [0, '#39'30%'#39'],'
      '      label: {'
      '        position: '#39'inner'#39','
      '        fontSize: 14'
      '      },'
      '      labelLine: {'
      '        show: false'
      '      },'
      '      data: ['
      '        { value: 1548, name: '#39'Search Engine'#39' },'
      '        { value: 775, name: '#39'Direct'#39' },'
      '        { value: 679, name: '#39'Marketing'#39', selected: true }'
      '      ]'
      '    },'
      '    {'
      '      name: '#39'Access From'#39','
      '      type: '#39'pie'#39','
      '      radius: ['#39'45%'#39', '#39'60%'#39'],'
      '      labelLine: {'
      '        length: 30'
      '      },'
      '      label: {'
      
        '        formatter: '#39'{a|{a}}{abg|}\n{hr|}\n  {b|{b}'#65306'}{c}  {per|{d' +
        '}%}  '#39','
      '        backgroundColor: '#39'#F6F8FC'#39','
      '        borderColor: '#39'#8C8D8E'#39','
      '        borderWidth: 1,'
      '        borderRadius: 4,'
      '        rich: {'
      '          a: {'
      '            color: '#39'#6E7079'#39','
      '            lineHeight: 22,'
      '            align: '#39'center'#39
      '          },'
      '          hr: {'
      '            borderColor: '#39'#8C8D8E'#39','
      '            width: '#39'100%'#39','
      '            borderWidth: 1,'
      '            height: 0'
      '          },'
      '          b: {'
      '            color: '#39'#4C5058'#39','
      '            fontSize: 14,'
      '            fontWeight: '#39'bold'#39','
      '            lineHeight: 33'
      '          },'
      '          per: {'
      '            color: '#39'#fff'#39','
      '            backgroundColor: '#39'#4C5058'#39','
      '            padding: [3, 4],'
      '            borderRadius: 4'
      '          }'
      '        }'
      '      },'
      '      data: ['
      '        { value: 1048, name: '#39'Baidu'#39' },'
      '        { value: 335, name: '#39'Direct'#39' },'
      '        { value: 310, name: '#39'Email'#39' },'
      '        { value: 251, name: '#39'Google'#39' },'
      '        { value: 234, name: '#39'Union Ads'#39' },'
      '        { value: 147, name: '#39'Bing'#39' },'
      '        { value: 135, name: '#39'Video Ads'#39' },'
      '        { value: 102, name: '#39'Others'#39' }'
      '      ]'
      '    }'
      '  ]'
      '}')
    TabOrder = 0
    ExplicitWidth = 414
    ExplicitHeight = 414
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 41
    Align = alTop
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    ExplicitWidth = 844
    object Button2: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 117
      Height = 33
      Hint = '{"type":"primary"}'
      Align = alLeft
      Caption = 'Update'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindow
      Font.Height = -17
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = Button2Click
    end
  end
end
