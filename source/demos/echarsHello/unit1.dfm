object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 490
  ClientWidth = 949
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = True
  OnMouseDown = FormMouseDown
  PixelsPerInch = 96
  TextHeight = 20
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 949
    Height = 490
    HelpType = htKeyword
    HelpKeyword = 'echarts'
    Align = alClient
    Lines.Strings = (
      '{'
      '  tooltip: {'
      '    trigger: '#39'item'#39','
      '    formatter: '#39'{a} <br/>{b}: {c} ({d}%)'#39
      '  },'
      '  legend: {'
      '    data: ['
      '      '#39'Direct'#39','
      '      '#39'Marketing'#39','
      '      '#39'Search Engine'#39','
      '      '#39'Email'#39','
      '      '#39'Union Ads'#39','
      '      '#39'Video Ads'#39','
      '      '#39'Baidu'#39','
      '      '#39'Google'#39','
      '      '#39'Bing'#39','
      '      '#39'Others'#39
      '    ]'
      '  },'
      '  series: ['
      '    {'
      '      name: '#39'Access From'#39','
      '      type: '#39'pie'#39','
      '      selectedMode: '#39'single'#39','
      '      radius: [0, '#39'30%'#39'],'
      '      label: {'
      '        position: '#39'inner'#39','
      '        fontSize: 14'
      '      },'
      '      labelLine: {'
      '        show: false'
      '      },'
      '      data: this.value0'
      '    },'
      '    {'
      '      name: '#39'Access From'#39','
      '      type: '#39'pie'#39','
      '      radius: ['#39'45%'#39', '#39'60%'#39'],'
      '      labelLine: {'
      '        length: 30'
      '      },'
      '      label: {'
      
        '        formatter: '#39'{a|{a}}{abg|}\n{hr|}\n  {b|{b}'#65306'}{c}  {per|{d' +
        '}%}  '#39','
      '        backgroundColor: '#39'#F6F8FC'#39','
      '        borderColor: '#39'#8C8D8E'#39','
      '        borderWidth: 1,'
      '        borderRadius: 4,'
      '        rich: {'
      '          a: {'
      '            color: '#39'#6E7079'#39','
      '            lineHeight: 22,'
      '            align: '#39'center'#39
      '          },'
      '          hr: {'
      '            borderColor: '#39'#8C8D8E'#39','
      '            width: '#39'100%'#39','
      '            borderWidth: 1,'
      '            height: 0'
      '          },'
      '          b: {'
      '            color: '#39'#4C5058'#39','
      '            fontSize: 14,'
      '            fontWeight: '#39'bold'#39','
      '            lineHeight: 33'
      '          },'
      '          per: {'
      '            color: '#39'#fff'#39','
      '            backgroundColor: '#39'#4C5058'#39','
      '            padding: [3, 4],'
      '            borderRadius: 4'
      '          }'
      '        }'
      '      },'
      '      data: this.value1'
      '    }'
      '  ]'
      '}')
    TabOrder = 0
  end
  object Button1: TButton
    Left = 8
    Top = 8
    Width = 105
    Height = 33
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 384
    Top = 112
  end
end
