﻿unit unit1;

interface

uses
     dwBase,
     //
     Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
     Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Data.DB,
  Data.Win.ADODB, Vcl.Menus, Vcl.Grids, Vcl.DBGrids;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Timer1: TTimer;
    Button1: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
     Form1             : TForm1;


implementation


{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
    sV0,sV1     : String;
    sJS         : String;
begin
    Randomize;

    //Get value0 string
    sV0 := 'this.value0=['
        +'{ value: %d, name: ''Search Engine'' },'
        +'{ value: %d, name: ''Direct'' },'
        +'{ value: %d, name: ''Marketing'', selected: true }'
    +'];';
    sV0 := Format(sV0,[Random(1500),Random(1000),Random(1000)]);

    //Get value1 string
    sV1 := 'this.value1=['
        +'{ value: %d, name: ''Baidu'' },'
        +'{ value: %d, name: ''Direct'' },'
        +'{ value: %d, name: ''Email'' },'
        +'{ value: %d, name: ''Google'' },'
        +'{ value: %d, name: ''Union Ads'' },'
        +'{ value: %d, name: ''Bing'' },'
        +'{ value: %d, name: ''Video Ads'' },'
        +'{ value: %d, name: ''Others'' }'
    +'];';
    sV1 := Format(sV1,[Random(1500),Random(1000),Random(1000),Random(1000),Random(1000),Random(1000),Random(1000),Random(1000)]);

    //
    dwRunJS(sV0+sV1,self);

    dwEcharts(Memo1);

end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    Timer1.OnTimer(nil);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
    sV0,sV1     : String;
    sJS         : String;
begin
    //value0
    //[
    //    { value: 1548, name: 'Search Engine' },
    //    { value: 775, name: 'Direct' },
    //    { value: 679, name: 'Marketing', selected: true }
    //]

    //value1
    //[
    //    { value: 1048, name: 'Baidu' },
    //    { value: 335, name: 'Direct' },
    //    { value: 310, name: 'Email' },
    //    { value: 251, name: 'Google' },
    //    { value: 234, name: 'Union Ads' },
    //    { value: 147, name: 'Bing' },
    //    { value: 135, name: 'Video Ads' },
    //    { value: 102, name: 'Others' }
    //]
    //

    //
    Randomize;

    //Get value0 string
    sV0 := 'this.value0=['
        +'{ value: %d, name: ''Search Engine'' },'
        +'{ value: %d, name: ''Direct'' },'
        +'{ value: %d, name: ''Marketing'', selected: true }'
    +'];';
    sV0 := Format(sV0,[Random(1500),Random(1000),Random(1000)]);

    //Get value1 string
    sV1 := 'this.value1=['
        +'{ value: %d, name: ''Baidu'' },'
        +'{ value: %d, name: ''Direct'' },'
        +'{ value: %d, name: ''Email'' },'
        +'{ value: %d, name: ''Google'' },'
        +'{ value: %d, name: ''Union Ads'' },'
        +'{ value: %d, name: ''Bing'' },'
        +'{ value: %d, name: ''Video Ads'' },'
        +'{ value: %d, name: ''Others'' }'
    +'];';
    sV1 := Format(sV1,[Random(1500),Random(1000),Random(1000),Random(1000),Random(1000),Random(1000),Random(1000),Random(1000)]);

    //
    dwRunJS(sV0+sV1,self);

    dwEcharts(Memo1);

end;

end.
