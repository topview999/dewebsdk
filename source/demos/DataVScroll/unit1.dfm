object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'DataV Scroll'
  ClientHeight = 652
  ClientWidth = 800
  Color = 4271650
  TransparentColorValue = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 20
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 311
    Width = 794
    Height = 328
    HelpKeyword = 'dvbox'
    HelpContext = 11
    Margins.Top = 30
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Options'
    Font.Charset = ANSI_CHARSET
    Font.Color = 13158600
    Font.Height = -15
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    VerticalAlignment = taAlignTop
    object Label1: TLabel
      Left = 52
      Top = 82
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'rowNum'
    end
    object Label2: TLabel
      Left = 52
      Top = 116
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'headerBGC'
    end
    object Label4: TLabel
      Left = 52
      Top = 150
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'oddRowBGC'#9
    end
    object Label5: TLabel
      Left = 52
      Top = 184
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'evenRowBGC'
    end
    object Label6: TLabel
      Left = 52
      Top = 220
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'waitTime'
    end
    object Label7: TLabel
      Left = 52
      Top = 256
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'headerHeight'
    end
    object Label8: TLabel
      Left = 324
      Top = 81
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'align'
    end
    object Label9: TLabel
      Left = 324
      Top = 117
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'index'
    end
    object Label10: TLabel
      Left = 324
      Top = 151
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'indexHeader'
    end
    object Label11: TLabel
      Left = 324
      Top = 185
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'carousel'
    end
    object Label12: TLabel
      Left = 324
      Top = 219
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'hoverPause'
    end
    object Label13: TLabel
      Left = 324
      Top = 253
      Width = 121
      Height = 20
      HelpContext = 10
      AutoSize = False
      Caption = 'header'
    end
    object SpinEdit_rowNum: TSpinEdit
      Left = 182
      Top = 80
      Width = 100
      Height = 30
      Hint = '{"dwstyle":"border:solid 1px #eee;border-radius:5px;"}'
      HelpContext = 10
      MaxValue = 8
      MinValue = 3
      TabOrder = 0
      Value = 5
      OnChange = UpdateOptions
    end
    object Edit_headerBGC: TEdit
      Left = 182
      Top = 116
      Width = 100
      Height = 28
      HelpContext = 10
      TabOrder = 1
      Text = '#00BAFF'
      OnChange = UpdateOptions
    end
    object Edit_oddRowBGC: TEdit
      Left = 182
      Top = 150
      Width = 100
      Height = 28
      HelpContext = 10
      TabOrder = 2
      Text = '#003B51'
      OnChange = UpdateOptions
    end
    object Edit_evenRowBGC: TEdit
      Left = 182
      Top = 184
      Width = 100
      Height = 28
      HelpContext = 10
      TabOrder = 3
      Text = '#0A2732'
      OnChange = UpdateOptions
    end
    object SpinEdit_WaitTime: TSpinEdit
      Left = 182
      Top = 218
      Width = 100
      Height = 30
      Hint = '{"dwstyle":"border:solid 1px #eee;border-radius:5px;"}'
      HelpContext = 10
      MaxValue = 8
      MinValue = 1
      TabOrder = 4
      Value = 3
      OnChange = UpdateOptions
    end
    object SpinEdit_HeaderHeight: TSpinEdit
      Left = 182
      Top = 254
      Width = 100
      Height = 30
      Hint = '{"dwstyle":"border:solid 1px #eee;border-radius:5px;"}'
      HelpContext = 10
      MaxValue = 50
      MinValue = 20
      TabOrder = 5
      Value = 35
      OnChange = UpdateOptions
    end
    object Edit_IndexHeader: TEdit
      Left = 451
      Top = 151
      Width = 100
      Height = 28
      HelpContext = 10
      TabOrder = 6
      Text = '#'
      OnChange = UpdateOptions
    end
    object ComboBox1: TComboBox
      Left = 452
      Top = 80
      Width = 60
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 7
      Text = 'left'
      OnChange = UpdateOptions
      Items.Strings = (
        'left'
        'center'
        'right')
    end
    object ComboBox2: TComboBox
      Left = 512
      Top = 80
      Width = 60
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 8
      Text = 'left'
      OnChange = UpdateOptions
      Items.Strings = (
        'left'
        'center'
        'right')
    end
    object ComboBox3: TComboBox
      Left = 572
      Top = 80
      Width = 60
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 9
      Text = 'left'
      OnChange = UpdateOptions
      Items.Strings = (
        'left'
        'center'
        'right')
    end
    object ComboBox4: TComboBox
      Left = 632
      Top = 80
      Width = 60
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 10
      Text = 'left'
      OnChange = UpdateOptions
      Items.Strings = (
        'left'
        'center'
        'right')
    end
    object ComboBox5: TComboBox
      Left = 692
      Top = 80
      Width = 60
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 11
      Text = 'left'
      OnChange = UpdateOptions
      Items.Strings = (
        'left'
        'center'
        'right')
    end
    object ComboBox_Index: TComboBox
      Left = 451
      Top = 114
      Width = 100
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 1
      TabOrder = 12
      Text = 'false'
      OnChange = UpdateOptions
      Items.Strings = (
        'true'
        'false')
    end
    object ComboBox_Carousel: TComboBox
      Left = 451
      Top = 185
      Width = 100
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 13
      Text = 'single'
      OnChange = UpdateOptions
      Items.Strings = (
        'single'
        'page')
    end
    object ComboBox_HoverPause: TComboBox
      Left = 451
      Top = 219
      Width = 100
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 14
      Text = 'true'
      OnChange = UpdateOptions
      Items.Strings = (
        'true'
        'false')
    end
    object ComboBox_Header: TComboBox
      Left = 451
      Top = 253
      Width = 100
      Height = 28
      HelpContext = 10
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 15
      Text = 'true'
      OnChange = UpdateOptions
      Items.Strings = (
        'true'
        'false')
    end
  end
  object Panel_0_Banner: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = 4271650
    ParentBackground = False
    TabOrder = 0
    object Label3: TLabel
      AlignWithMargins = True
      Left = 210
      Top = 3
      Width = 367
      Height = 44
      Margins.Left = 10
      Align = alLeft
      AutoSize = False
      Caption = 'DataV Scroll'
      Color = 4210752
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
    end
    object Panel_Title: TPanel
      Left = 0
      Top = 0
      Width = 200
      Height = 50
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -29
      Font.Name = 'Verdana'
      Font.Style = [fsBold, fsItalic]
      ParentBackground = False
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object Label_Title: TLabel
        Left = 0
        Top = 0
        Width = 200
        Height = 50
        HelpType = htKeyword
        Align = alClient
        Alignment = taCenter
        Caption = 'DeWeb'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Layout = tlCenter
        WordWrap = True
        ExplicitWidth = 96
        ExplicitHeight = 29
      end
    end
  end
  object StringGrid1: TStringGrid
    Left = 0
    Top = 50
    Width = 800
    Height = 231
    HelpType = htKeyword
    HelpKeyword = 'dvscroll'
    Align = alTop
    RowCount = 10
    TabOrder = 2
  end
end
