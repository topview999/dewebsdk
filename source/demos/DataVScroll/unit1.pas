﻿unit unit1;

interface

uses
     dwBase,
     SynCommons,

     //
     Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
     Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Data.DB,
  Data.Win.ADODB, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, Vcl.Samples.Spin;

type
  TForm1 = class(TForm)
    Panel_0_Banner: TPanel;
    Label3: TLabel;
    Panel_Title: TPanel;
    Label_Title: TLabel;
    Panel1: TPanel;
    StringGrid1: TStringGrid;
    Label1: TLabel;
    SpinEdit_rowNum: TSpinEdit;
    Label2: TLabel;
    Edit_headerBGC: TEdit;
    Label4: TLabel;
    Edit_oddRowBGC: TEdit;
    Label5: TLabel;
    Edit_evenRowBGC: TEdit;
    Label6: TLabel;
    SpinEdit_WaitTime: TSpinEdit;
    Label7: TLabel;
    SpinEdit_HeaderHeight: TSpinEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Edit_IndexHeader: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    ComboBox5: TComboBox;
    ComboBox_Index: TComboBox;
    ComboBox_Carousel: TComboBox;
    ComboBox_HoverPause: TComboBox;
    Label13: TLabel;
    ComboBox_Header: TComboBox;
    procedure FormCreate(Sender: TObject);
    //
    procedure UpdateOptions(Sender: TObject);
  private
    { Private declarations }
  public
  end;

var
     Form1             : TForm1;


implementation


{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
     iR,iC     : Integer;
begin

     //
     Top  := 0;
     //
     with StringGrid1 do begin
          Cells[0,0]   := '日期';
          Cells[1,0]   := '姓名';
          Cells[2,0]   := '省份';
          Cells[3,0]   := '城市';
          Cells[4,0]   := '详细地址';
          //
          Cells[0,1]   := '2000-01-01';
          Cells[1,1]   := '张云政';
          Cells[2,1]   := '湖北';
          Cells[3,1]   := '襄阳';
          Cells[4,1]   := '电力局办公室01001号';
          //
          Cells[0,2]   := '2001-07-01';
          Cells[1,2]   := '李景学';
          Cells[2,2]   := '北京';
          Cells[3,2]   := '朝阳区';
          Cells[4,2]   := '烟草公司驻外楼2901室';
          //
          Cells[0,3]   := '2002-12-01';
          Cells[1,3]   := '周子琴';
          Cells[2,3]   := '上海';
          Cells[3,3]   := '淞沪区';
          Cells[4,3]   := '外滩管理事务局管理处';
          //
          Cells[0,4]   := '2006-03-01';
          Cells[1,4]   := '谢玲芳';
          Cells[2,4]   := '辽宁';
          Cells[3,4]   := '沈阳';
          Cells[4,4]   := '民航管理中心监察室9901';
          //
          Cells[0,5]   := '2007-03-01';
          Cells[1,5]   := '张曼玉';
          Cells[2,5]   := '香港';
          Cells[3,5]   := '九龙';
          Cells[4,5]   := '无线集团';
          //
          Cells[0,6]   := '2008-03-01';
          Cells[1,6]   := '刘德华';
          Cells[2,6]   := '甘肃';
          Cells[3,6]   := '兰州';
          Cells[4,6]   := '房地产公司驻外楼1212室';
          //
          Cells[0,7]   := '2009-03-01';
          Cells[1,7]   := '梁朝伟';
          Cells[2,7]   := '山东';
          Cells[3,7]   := '济南';
          Cells[4,7]   := '数据中心';
          //
          Cells[0,8]   := '2010-03-01';
          Cells[1,8]   := '张益';
          Cells[2,8]   := '辽宁';
          Cells[3,8]   := '大连';
          Cells[4,8]   := '金融管理大队';
          //
          Cells[0,9]   := '2011-03-01';
          Cells[1,9]   := '贾玲';
          Cells[2,9]   := '湖北';
          Cells[3,9]   := '宜城';
          Cells[4,9]   := '综合演艺管理中心';
          //
          ColWidths[0]   := 140;
          ColWidths[1]   := 140;
          ColWidths[2]   := 140;
          ColWidths[3]   := 140;
          ColWidths[4]   := 200;
     end;
end;

procedure TForm1.UpdateOptions(Sender: TObject);
var
    joHint  : Variant;
begin
    //取得Hint属性对象
    joHint  := _json(StringGrid1.Hint);
    if joHint = unassigned then begin
        joHint  := _json('{}');
    end;

    //header	表头数据	Array<String>	---	[]
    //data	表数据	Array<Array>	---	[]

    //rowNum	表行数	Number	---	5
    joHint.rowNum       := SpinEdit_RowNum.Value;

    //headerBGC	表头背景色	String	---	'#00BAFF'
    joHint.headerBGC    := Edit_headerBGC.Text;

    //oddRowBGC	奇数行背景色	String	---	'#003B51'
    joHint.oddRowBGC    := Edit_oddRowBGC.Text;

    //evenRowBGC	偶数行背景色	String	---	#0A2732
    joHint.evenRowBGC    := Edit_evenRowBGC.Text;

    //waitTime	轮播时间间隔(ms)	Number	---	2000
    joHint.waitTime      := SpinEdit_waitTime.Value*1000;

    //headerHeight	表头高度	Number	---	35
    joHint.headerHeight  := SpinEdit_headerHeight.Value;

    //columnWidth	列宽度	Array<Number>	[1]	[]


    //align	列对齐方式	Array<String>	[2]	[]
    joHint.align       := '["'+ComboBox1.Text+'"'
            +',"'+ComboBox2.Text+'"'
            +',"'+ComboBox3.Text+'"'
            +',"'+ComboBox4.Text+'"'
            +',"'+ComboBox5.Text+'"]';


    //index	显示行号	Boolean	true|false	false
    joHint.index    := ComboBox_Index.text;
    if joHint.index = 'true' then begin
        StringGrid1.ColWidths[0]    := 50;
        StringGrid1.ColWidths[4]    := 140;
    end else begin
        StringGrid1.ColWidths[0]    := 140;
        StringGrid1.ColWidths[4]    := 200;
    end;

    //indexHeader	行号表头	String	-	'#'
    joHint.indexHeader  := Edit_indexHeader.Text;

    //carousel	轮播方式	String	'single'|'page'	'single'
    joHint.carousel    := ComboBox_carousel.Text;

    //hoverPause	悬浮暂停轮播	Boolean	---	true
    joHint.hoverPause    := ComboBox_hoverPause.ItemIndex=0;

    //Header
    if ComboBox_Header.Text = 'true' then begin
        StringGrid1.FixedRows   := 1;
    end else begin
        StringGrid1.FixedRows   := 0;
    end;

    //
    StringGrid1.Hint    := joHint;
end;

end.
