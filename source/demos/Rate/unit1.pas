﻿unit unit1;

interface

uses
    //
    dwBase,

    //
    CloneComponents,
    SynCommons,

    //
    Math,
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.Buttons;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    Edit1: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.Button1Click(Sender: TObject);
var
    fValue  : Double;
begin
    fValue  := StrToFloatDef(Edit1.Text,0);
    fValue  := fValue + 0.2;
    fValue  := Max(0,Min(5,fValue));
    Edit1.Text  := Format('%.1f',[fValue]);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
    dwMessage(Edit1.Text,'success',self);
end;

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
    Edit1.Enabled   := not CheckBox1.Checked;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
    Label1.Caption  := 'OnChange : '+(Edit1.Text);
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    dwSetMobileMode(Self,360,720);
end;

end.
