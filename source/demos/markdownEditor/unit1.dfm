object Form1: TForm1
  Left = 0
  Top = 0
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 640
  ClientWidth = 1000
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 21
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 640
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 998
      Height = 40
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = 'DeWeb MarkDown '#32534#36753#22120
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      ExplicitLeft = 0
      ExplicitTop = -2
      ExplicitWidth = 598
    end
    object ComboBox1: TComboBox
      AlignWithMargins = True
      Left = 4
      Top = 601
      Width = 992
      Height = 35
      Align = alBottom
      Style = csDropDownList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = ComboBox1Change
      Items.Strings = (
        'Memo1__obj.hide();//'#38544#34255#32534#36753#22120
        'Memo1__obj.show();//'#26174#31034#32534#36753#22120
        'Memo1__obj.gotoLine(19);//'#36716#21040#31532'19'#34892
        'alert(Memo1__obj.getMarkdown());//'#33719#21462#32534#36753#22120#20869#23481#65288#19981#21547'html'#65289
        'alert(Memo1__obj.getHTML());//'#33719#21462#32534#36753#22120'html'#20869#23481
        'Memo1__obj.previewing();//'#39044#35272#25928#26524
        'Memo1__obj.fullscreen();//'#20840#23631#65288#25353'ESC'#21462#28040#65289
        'Memo1__obj.hideToolbar();//'#38544#34255#24037#20855#26639
        'Memo1__obj.showToolbar();//'#26174#31034#24037#20855#26639)
    end
    object Button1: TButton
      Left = 12
      Top = 3
      Width = 75
      Height = 35
      Hint = '{"type":"primary"}'
      Caption = 'Set'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 93
      Top = 3
      Width = 75
      Height = 35
      Hint = '{"type":"primary"}'
      Caption = 'Get'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = Button2Click
    end
    object Memo1: TMemo
      Left = 1
      Top = 41
      Width = 607
      Height = 557
      HelpType = htKeyword
      HelpKeyword = 'markdown'
      Align = alClient
      Lines.Strings = (
        '[TOC]'
        '#### Disabled options'
        ''
        '- TeX (Based on KaTeX);'
        '- Emoji;'
        '- Task lists;'
        '- HTML tags decode;'
        '- Flowchart and Sequence Diagram;'
        ''
        '#### Editor.md directory'
        ''
        '    editor.md/'
        '            lib/'
        '            css/'
        '            scss/'
        '            tests/'
        '            fonts/'
        '            images/'
        '            plugins/'
        '            examples/'
        '            languages/     '
        '            editormd.js'
        '            ...'
        ''
        '```html'
        '&lt;!-- English --&gt;'
        '&lt;script src="../dist/js/languages/en.js"&gt;&lt;/script&gt;'
        ''
        '&lt;!-- '#32321#39636#20013#25991' --&gt;'
        
          '&lt;script src="../dist/js/languages/zh-tw.js"&gt;&lt;/script&gt' +
          ';'
        '```')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      ExplicitLeft = -5
      ExplicitTop = 38
    end
    object Memo2: TMemo
      Left = 608
      Top = 41
      Width = 391
      Height = 557
      Align = alRight
      Lines.Strings = (
        'Memo1')
      TabOrder = 4
    end
  end
end
