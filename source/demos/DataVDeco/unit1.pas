﻿unit unit1;

interface

uses
    //
    dwBase,


    //
    Math,
    Graphics,strutils,
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.Buttons,  Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    Panel01: TPanel;
    Panel06: TPanel;
    Panel02: TPanel;
    Panel04: TPanel;
    Panel08: TPanel;
    Panel05: TPanel;
    Panel07: TPanel;
    Panel09: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    //刷新以启动动画
    DockSite        := True;
    //清除启动事件，以避免循环执行
    OnMouseDown    := nil;
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    dwSetMobileMode(self,360,720);
    Height  := 1000;
    //
    Panel09.Margins.Left    := (Width-160) div 2;
    Panel09.Margins.Right   := (Width-160) div 2;
    Panel12.Margins.Left    := (Width-200) div 2;
    Panel12.Margins.Right   := (Width-200) div 2;
end;

end.
