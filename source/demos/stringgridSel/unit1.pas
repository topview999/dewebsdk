﻿unit unit1;

interface

uses
     //
     dwBase,
     dwSGUnit,      //StringGrid控制单元

     
     //
     SynCommons,

     //
     Math,
     Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, jpeg, ExtCtrls,  Vcl.Grids,
     Vcl.Imaging.pngimage;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    Panel_1_ToolBar: TPanel;
    Button_Get: TButton;
    Button_Set: TButton;
    Panel_0_Banner: TPanel;
    Panel_Title: TPanel;
    Image2: TImage;
    Label1: TLabel;
    Panel_9_StatusBar: TPanel;
    Label_Status: TLabel;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button_GetClick(Sender: TObject);
    procedure Button_SetClick(Sender: TObject);
    procedure StringGrid1GetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
    procedure Button1Click(Sender: TObject);
    procedure StringGrid1Click(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
  private
  public

  end;

var
     Form1     : TForm1;


implementation


{$R *.dfm}




procedure TForm1.Button1Click(Sender: TObject);
begin
     //取消第二行为选中状态
     dwSGSetRowChecked(StringGrid1,2,False);

end;

procedure TForm1.Button_GetClick(Sender: TObject);
begin
     //取第二行的选中状态，并显示
     dwMessage(dwBoolToStr(dwSGGetRowChecked(StringGrid1,2)),'success',self);
end;

procedure TForm1.Button_SetClick(Sender: TObject);
begin
     //置第二行为选中状态
     dwSGSetRowChecked(StringGrid1,2,True);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
     iR,iC     : Integer;
begin

     //
     Top  := 0;
     //
     with StringGrid1 do begin
          Cells[0,0]   := '{"caption":"日期","type":"selection"}';
          Cells[1,0]   := '姓名';
          Cells[2,0]   := '省份';
          Cells[3,0]   := '城市';
          Cells[4,0]   := '详细地址';
          //
          Cells[0,1]   := '2000-01-01';
          Cells[1,1]   := '张云政';
          Cells[2,1]   := '湖北';
          Cells[3,1]   := '襄阳';
          Cells[4,1]   := '电力局办公室';
          //
          Cells[0,2]   := '2001-07-01';
          Cells[1,2]   := '李景学';
          Cells[2,2]   := '北京';
          Cells[3,2]   := '朝阳区';
          Cells[4,2]   := '烟草公司驻外楼';
          //
          Cells[0,3]   := '2002-12-01';
          Cells[1,3]   := '周子琴';
          Cells[2,3]   := '上海';
          Cells[3,3]   := '淞沪区';
          Cells[4,3]   := '外滩管理处';
          //
          Cells[0,4]   := '2006-03-01';
          Cells[1,4]   := '谢玲芳';
          Cells[2,4]   := '辽宁';
          Cells[3,4]   := '沈阳';
          Cells[4,4]   := '民航管理中心室';
          //
          Cells[0,5]   := '2007-03-01';
          Cells[1,5]   := '张曼玉';
          Cells[2,5]   := '香港';
          Cells[3,5]   := '九龙';
          Cells[4,5]   := '无线集团';
          //
          Cells[0,6]   := '2008-03-01';
          Cells[1,6]   := '刘德华';
          Cells[2,6]   := '甘肃';
          Cells[3,6]   := '兰州';
          Cells[4,6]   := '房地产公司外楼';
          //
          Cells[0,7]   := '2009-03-01';
          Cells[1,7]   := '梁朝伟';
          Cells[2,7]   := '山东';
          Cells[3,7]   := '济南';
          Cells[4,7]   := '数据中心';
          //
          Cells[0,8]   := '2010-03-01';
          Cells[1,8]   := '张益';
          Cells[2,8]   := '辽宁';
          Cells[3,8]   := '大连';
          Cells[4,8]   := '金融管理大队';
          //
          Cells[0,9]   := '2011-03-01';
          Cells[1,9]   := '贾玲';
          Cells[2,9]   := '湖北';
          Cells[3,9]   := '宜城';
          Cells[4,9]   := '综合演艺中心';
          //
          ColWidths[0]   := 40;
          ColWidths[1]   := 70;
          ColWidths[2]   := 50;
          ColWidths[3]   := 70;
          ColWidths[4]   := 130;
     end;

end;



procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    if (X<600)or(Y<600) then begin
        dwSetMobileMode(Self,360,720);
    end;
    with StringGrid1 do begin
        ColWidths[4]   := self.Width - 231;
    end;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
     //置第二行为选中状态
     dwSGSetRowChecked(StringGrid1,2,True);
end;

procedure TForm1.StringGrid1Click(Sender: TObject);
begin
    var iRow := StringGrid1.Row;
    dwSGSetRowChecked(StringGrid1,iRow,not dwSGGetRowChecked(StringGrid1,iRow));

end;

procedure TForm1.StringGrid1GetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
var
    joHint  : Variant;
    joValue : Variant;
begin
    //将参数Value 转换为JSON对象
    joValue := _json(Value);
    //检查是否为空
    if joValue = unassigned then begin
        joValue := _json('{}');
    end;

    //检查JSON对象中是否有指定的属性
    if not joValue.Exists('type') then begin
        Exit;
    end;

    //检查事件来源
    if joValue.type = 'selection' then begin
        //为'selection'表明该事件是由选中或取消选中激发的


        //每行的选择状态保存在当前StringGrid的Hint中，先将该Hint转化为JSON对象（需要uses SysCommons）
        joHint  := _json(StringGrid1.Hint);

        //显示选中行。 以数组形式保存在 JSON对象的__selection属性中
        Label_Status.Caption    := '当前的选中行：' + VariantSaveJSON(joHint.__selection);
    end;
end;

end.
