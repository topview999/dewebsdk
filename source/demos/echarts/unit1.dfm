object Form1: TForm1
  Left = 0
  Top = 0
  HelpType = htKeyword
  Margins.Top = 20
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 541
  ClientWidth = 844
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -17
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnMouseDown = FormMouseDown
  PixelsPerInch = 96
  TextHeight = 23
  object Memo1: TMemo
    AlignWithMargins = True
    Left = 3
    Top = 44
    Width = 414
    Height = 414
    HelpType = htKeyword
    HelpKeyword = 'echarts'
    Lines.Strings = (
      '{'
      '  series: ['
      '    {'
      '      type: '#39'gauge'#39','
      '      axisLine: {'
      '        lineStyle: {'
      '          width: 30,'
      '          color: ['
      '            [0.3, '#39'#67e0e3'#39'],'
      '            [0.7, '#39'#37a2da'#39'],'
      '            [1, '#39'#fd666d'#39']'
      '          ]'
      '        }'
      '      },'
      '      pointer: {'
      '        itemStyle: {'
      '          color: '#39'auto'#39
      '        }'
      '      },'
      '      axisTick: {'
      '        distance: -30,'
      '        length: 8,'
      '        lineStyle: {'
      '          color: '#39'#fff'#39','
      '          width: 2'
      '        }'
      '      },'
      '      splitLine: {'
      '        distance: -30,'
      '        length: 30,'
      '        lineStyle: {'
      '          color: '#39'#fff'#39','
      '          width: 4'
      '        }'
      '      },'
      '      axisLabel: {'
      '        color: '#39'auto'#39','
      '        distance: 40,'
      '        fontSize: 20'
      '      },'
      '      detail: {'
      '        valueAnimation: true,'
      '        formatter: '#39'{value} km/h'#39','
      '        color: '#39'auto'#39
      '      },'
      '      data: ['
      '        {'
      '          value: this.value0'
      '        }'
      '      ]'
      '    }'
      '  ]'
      '}')
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 844
    Height = 41
    Align = alTop
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    object Button2: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 117
      Height = 33
      Hint = '{"type":"primary"}'
      Align = alLeft
      Caption = 'Update'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindow
      Font.Height = -17
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object Memo2: TMemo
    AlignWithMargins = True
    Left = 423
    Top = 44
    Width = 414
    Height = 414
    HelpType = htKeyword
    HelpKeyword = 'echarts'
    Lines.Strings = (
      '{'
      '  xAxis: {'
      '    type: '#39'category'#39','
      '    data: this.valuex'
      '  },'
      '  yAxis: {'
      '    type: '#39'value'#39
      '  },'
      '  series: ['
      '    {'
      '      data:this.valuey,'
      '      type: '#39'line'#39
      '    }'
      '  ]'
      '}')
    TabOrder = 2
  end
end
