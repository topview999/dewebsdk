﻿unit unit1;

interface

uses
    //
    dwBase,

    //
    CloneComponents,


    //
    Math,
    Graphics,strutils,
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.Buttons,  Vcl.ComCtrls, Vcl.Menus, VclTee.TeeGDIPlus,
  VCLTee.TeEngine, VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart, Data.DB, Vcl.DBGrids,
  Vcl.Samples.Gauges;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    Button2: TButton;
    Memo2: TMemo;
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    gsMainDir   : String;
    sNew        : string;
        procedure UpdateDatas;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
function dwEcharts(ACtrl:TComponent):Integer;
var
    sJS     : String;
begin
    sJS     := 'var oEcharts = echarts.init(document.getElementById("'+dwFullName(ACtrl)+'"));'
            +'var oOption = '+TMemo(ACtrl).Lines.Text+';'
            +'oEcharts.setOption(oOption);';
    dwRunJS(sJS,TForm(ACtrl.Owner));
    //
    Result  := 0;
end;


procedure TForm1.Button2Click(Sender: TObject);
begin
    UpdateDatas;
end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
begin
    UpdateDatas;

end;

procedure TForm1.UpdateDatas;
var
    sJS     : String;
    I       : Integer;
begin
    Randomize;
    sJS := 'this.value0='+IntToStr(Random(100))+';';
    //
    sJS := sJS + 'this.valuex=[';
    for I := 0 to 9 do begin
        sJS := sJS + ''''+IntToStr(I)+''',';
    end;
    sJS := sJS + '];';
    //
    sJS := sJS + 'this.valuey=[';
    for I := 0 to 9 do begin
        sJS := sJS + ''''+IntToStr(random(100))+''',';
    end;
    sJS := sJS + '];';
    //
    dwRunJS(sJS,self);
    dwEcharts(Memo1);
    dwEcharts(Memo2);
end;

end.
