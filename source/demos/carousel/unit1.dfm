object Form1: TForm1
  Left = 0
  Top = 0
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 480
  ClientWidth = 390
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = 16448250
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 20
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 390
    Height = 65
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'deweb : delphi-web'#35299#20915#26041#26696
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = #24494#36719#38597#40657
    Font.Style = [fsBold]
    ParentFont = False
    Layout = tlCenter
    ExplicitWidth = 320
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 65
    Width = 390
    Height = 216
    HelpType = htKeyword
    HelpKeyword = 'carousel'
    ActivePage = TabSheet3
    Align = alTop
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      TabVisible = False
      object Panel1: TPanel
        AlignWithMargins = True
        Left = 4
        Top = 0
        Width = 382
        Height = 206
        Hint = '{"radius":"5px"}'
        Margins.Left = 4
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        BevelOuter = bvNone
        Color = clNavy
        ParentBackground = False
        TabOrder = 0
        ExplicitLeft = 0
        object Label2: TLabel
          Left = 82
          Top = 20
          Width = 150
          Height = 28
          AutoSize = False
          Caption = #22269#20135#35299#20915#26041#26696
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -21
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold, fsItalic]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 115
          Top = 80
          Width = 150
          Height = 38
          AutoSize = False
          Caption = #22269#20135#31934#21697
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -27
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 184
          Top = 147
          Width = 105
          Height = 28
          AutoSize = False
          Caption = #21280#24515#25171#36896
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -21
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold, fsItalic]
          ParentFont = False
        end
        object Image1: TImage
          Left = 16
          Top = 12
          Width = 48
          Height = 48
          Hint = '{"src":"media/images/logo.png"}'
          AutoSize = True
          Picture.Data = {
            0954506E67496D61676589504E470D0A1A0A0000000D49484452000000300000
            003008060000005702F9870000065E4944415478DAD5594D561B391096DA6ABF
            C9B0809C20649784459C13604E10E70498136096137B1ECD1B9859C69C20F609
            3027009F00676160379E13C45EC064E876D794BA5BB6A45677CB76320C7A8F67
            FA4FAAAF7EBE2A952879E2833EB6004F06C09B13FF9010A85342378140E7DE77
            0F461E1DFF6F00BCFA03364BD3E9F6B454EADFFE42479AF01E2E74A87C00E472
            D872771E150017DA09B96649956B96DF9B8664E7F657F75205F07085CF2BFAF7
            40488F00ED5CB7D8F97F0AE0D56F7ED571601785AAEBCF4C00B64E7C7EBD9D35
            1FBAD48882E30D5BACFB43016C7AB0B1C68243FCAA61783C2100BD69C9F57417
            7AFD7B507300CE0A1740B722941D0C9B74F0DD016C9D4005887F265C65BE2874
            0975DB458B721034C420A6640397EDDCF9A5DE33E663509306A5F485FC6E48A0
            71D32C9F7E37005B27411D25FDAC0B6ED2F632E3F5F143C3A1D4C37FD767D323
            535D37CB7B2B0330083F0929ADDF7C64BDA26F7990F35F1B9011214C83365AE8
            FD5C49C54C950B20253CC017D47ACD24108F0FC1EB6F8EFD9E2288245048A107
            8E7B9E056AEBF8A14328DD9D7F926F894C00DCE70909AE64E1EF02B7AA271F2E
            F8CF6EB04F81348092CB7B9F458BAD31641E4ADF66CDCF29941276648A1D5D71
            F8EED175D3F5AC01C442F91733EECE103E629710178A0273A6B10166D91D2B10
            40CEC3126B98AC81D4DBC69F7D716DA2E74C00DAC793A9C32AFA22BAA955EDCE
            41A0751A14301628AD913848FB82856485C82E2886EC8A7C4E74A5778500A292
            200CFE945ED91B3659C75678699C0E9B6E439D976C0897E1D734F4DF3B80C028
            A90AD03AA835371825C08DAE9402A009D74721AA8A564C758D6184000737AD72
            5BBEC7057AE6FA98C149DD5C5AA4412414FB29793E422BBCCC04A06B5FF7BB54
            60E701A0F4834EB5C67C924201DD61AB5C579476FC309A273BD523140032DA55
            B49FC51A96AE97529C6605251614006AD195F6FD422190ADB0ACA8733F8FAC05
            C12ED636DD3B9F8CB85B58592096524960492C7C15D7773E7B2EDC8CDABC2404
            472A6D4482984048A637594A6815BF1DE451EBEC7D87BD94994F56AEEC9E3300
            BC442E39E44268128599059900C7CD173AEE073E715C619646B170C106172ED9
            1F9C9902546855592767E824A02965C670D4F482EEC38AE9818CD12D76F40C9A
            B8CC859CD45242259A538332732831A8019F3DB302A0BB84292B6A04601E8915
            6C6241A7CC420072F6D5CDA7A7F52C8EC7D2A15DC432B35838F1797CADE7BD8B
            42EA240309BA312AE2B90E806B74DBA4E1F496506528AE1DC2D888C746BCB9F7
            3DA974D0541B07BB0DA56602909ECD5D482981550195C58400DC0D00E27B580A
            2466EF60901F712051D6CDD871F1C56D92E262006C6220A94AD75CB29917B011
            90907685151361EB005043301BA2382CB2820C40068CF3FC75DD2A6F2A009420
            D4D2F93CE8D83B9E94B0D4BE4AED8DCD4046A88CF6DFBEDBCD6A626506B446E5
            8541ACE601C217EB60A01CF0CB64D3D2E056D103DA7A20034D811C996A7A9B51
            98073204534B62CB24943126C85E9ECE5E625E522220DFBBFDE8F6E5EBC24CBC
            75EC7F4DF9B444579116EC12903A05FA2BA58E67AAAB0C9A151F29EEC387DCB9
            30D64226E1E46089DC88F9036B005161E7B485E071378FECE3823539C84D5D3B
            53292E645873A73563396DCAA47AC2B2ECB029AEC283144878A8073DDF0F5FB7
            DC9A6E0171DFD6C2EA7E20E99E451339B463D2425E10F3C5EF0356E7E64DFAA7
            9F8C855D3C2226516A7DB4F87DE0561669BB2FD5DC35F0F7ACD99530D667EE2A
            45F3709E97886182345D1545229FE7A772F0F6DB03FB920768E9F6BA04A28F89
            A93E2BB1B5368B15000A6DB111E2F7D3EE6CDE5CAD04205A0805165ACFE95AE7
            03C0DAE9DB3F642CB49C5562E81B1C6B00D22106C96AF12581C8855F2F9A4F1A
            A93DB734576ADFBD50636B26BC96B8F2CEB604C545F50E891A59A66D234F4EE8
            26AC93D58E470067A6F859DA028666EB8012776F9143089B91B71D35ED3FAC01
            44200CD48979DFBBF7D9E9AA278D71FC4CB18CE781AC04FF29AE31A684F5F294
            B5C0094D54357220733FC752032869870EEB2E7AD02177B535C127E8EF35DBA2
            6F21164A3A771D623AB0C36A93B7D743FC33717774D801C10B0A5011FD50C312
            7DAC736A3F3C91C5193B6C2F5AD8E58C3E6ADD5BA6D45EED9C384E42759B76A1
            61C4A79A403BCBEE115606A083C16AB38AAE54894F2225378B2A533A86A8A744
            064581F928001E6B3C7900FF026484B86D9717D4A50000000049454E44AE4260
            82}
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      TabVisible = False
      object Panel2: TPanel
        AlignWithMargins = True
        Left = 4
        Top = 0
        Width = 382
        Height = 206
        Hint = '{"radius":"5px"}'
        Margins.Left = 4
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        BevelOuter = bvNone
        Color = clLime
        ParentBackground = False
        TabOrder = 0
        ExplicitLeft = 0
        object Label5: TLabel
          Left = 32
          Top = 24
          Width = 200
          Height = 28
          AutoSize = False
          Caption = #26356#31616#27905#30340#35299#20915#26041#26696
          Font.Charset = ANSI_CHARSET
          Font.Color = 6579300
          Font.Height = -21
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 98
          Top = 71
          Width = 217
          Height = 28
          AutoSize = False
          Caption = #26080#38656#35201#23433#35013#25511#20214
          Font.Charset = ANSI_CHARSET
          Font.Color = 6579300
          Font.Height = -21
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold, fsItalic]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 43
          Top = 129
          Width = 214
          Height = 28
          AutoSize = False
          Caption = #30452#25509#32534#35793#65292#25152#35265#21363#25152#24471
          Font.Charset = ANSI_CHARSET
          Font.Color = 6579300
          Font.Height = -21
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold, fsItalic]
          ParentFont = False
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      TabVisible = False
      object Panel3: TPanel
        AlignWithMargins = True
        Left = 4
        Top = 0
        Width = 382
        Height = 206
        Hint = '{"radius":"5px"}'
        Margins.Left = 4
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        BevelOuter = bvNone
        Color = clRed
        ParentBackground = False
        TabOrder = 0
        ExplicitLeft = 0
        object Label8: TLabel
          Left = 32
          Top = 24
          Width = 193
          Height = 28
          AutoSize = False
          Caption = #26356#23436#32654#30340#35299#20915#26041#26696
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -21
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 88
          Top = 71
          Width = 217
          Height = 28
          AutoSize = False
          Caption = #25903#25345#25163#26426#12289#24179#26495#12289#30005#33041
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -21
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold, fsItalic]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 32
          Top = 128
          Width = 185
          Height = 28
          AutoSize = False
          Caption = #19968#22871#20195#30721#23436#32654#35299#20915
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -21
          Font.Name = #24494#36719#38597#40657
          Font.Style = [fsBold, fsItalic]
          ParentFont = False
        end
        object Button1: TButton
          Left = 264
          Top = 128
          Width = 66
          Height = 30
          Hint = '{"type":"success"}'
          Caption = #19979#36733
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindow
          Font.Height = -15
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = Button1Click
        end
      end
    end
  end
end
