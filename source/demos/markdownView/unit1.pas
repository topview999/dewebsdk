﻿unit unit1;

interface

uses
    //
    dwBase,

    //
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    DateUtils,SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    ComboBox1: TComboBox;
    Label1: TLabel;
    Button1: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

function dwSetMarkdown(AMemo:TMemo;AText:String;AForm:TForm):Integer;
begin
    AMemo.Text  := AText;
    //
    AText   := StringReplace(AText,#13#10,'\n',[rfReplaceAll]);
    AText   := StringReplace(AText,#13,'\n',[rfReplaceAll]);
    AText   := dwPrefix(AMemo)+AMemo.Name+'__obj.setMarkdown('''+AText+''');';
    dwRunJS(AText,AForm);
end;


procedure TForm1.Button1Click(Sender: TObject);
var
    sText   : string;
begin
    sText   := '#### Disabled options'#13
        +'- TeX (Based on KaTeX);'#13
        +'- Emoji;'#13
        +'- Task lists;'#13
        +'- HTML tags decode;'#13
        +'- Flowchart and Sequence Diagram;'#13
        +'#### Editor.md directory'#13
        +'- Flowchart and Sequence Diagram;';
    //
    //dwRunJS('re__obj.setMarkdown(''#### Disabled options\n- TeX (Based on KaTeX);\n- Emoji;\n- Task lists;- HTML tags decode;'
    //    +'\n- Flowchart and Sequence Diagram;\n#### Editor.md directory'');',self);

    //
    Memo1.Text  := sText;
    //dwSetMarkdown(Memo1,sText,self);

end;

end.
