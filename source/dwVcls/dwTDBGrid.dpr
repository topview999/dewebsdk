﻿library dwTDBGrid;

uses
    System.ShareMem,      //必须添加
    dwCtrlBase,           //一些基础函数
    SynCommons,           //mormot用于解析JSON的单元
    //untLog,             //日志
    System.SysUtils,
    System.DateUtils,
    Vcl.ComCtrls,
    Vcl.ExtCtrls,
    System.Classes,
    Data.DB,
    Vcl.DBGrids,
    Vcl.Dialogs,
    Vcl.StdCtrls,
    Winapi.Windows,
    Vcl.Controls,
    Vcl.Forms;

function DeleteLastStr(str: string): string;
begin
    Delete(str, Length(str), 1);
    Result := str;
end;

function _GetValue(AField: TField): string;
begin
    try
        if AField.DataType in [ftString, ftSmallint, ftInteger, ftWord, ftBoolean, ftFloat, ftCurrency, ftBCD, ftBytes, ftVarBytes, ftAutoInc, ftFmtMemo, ftFixedChar, ftWideString, ftLargeint, ftMemo] then
        begin
            Result := dwProcessCaption(AField.AsString);
        end else if AField.DataType in [ftDate] then begin
                    Result := FormatDateTime('yyyy-mm-dd', AField.AsDateTime);
        end else if AField.DataType in [ftTime] then begin
            Result := FormatDateTime('HH:MM:SS', AField.AsDateTime);
        end else if AField.DataType in [ftDateTime] then begin
            case AField.Tag of
                1 : begin
                    Result := FormatDateTime('yyyy-mm-dd', AField.AsDateTime);
                end;
                2 : begin
                    Result := FormatDateTime('HH:MM:SS', AField.AsDateTime);
                end;
            else
                Result := FormatDateTime('yyyy-mm-dd HH:MM:SS', AField.AsDateTime);
            end;
        end else begin
            Result := '';
        end;
  except
  end;
end;

//---------------------以上为辅助函数---------------------------------------------------------------

//当前控件需要引入的第三方JS/CSS ,一般为不做改动,目前仅在TChart使用时需要用到
function dwGetExtra(ACtrl: TComponent): string; stdcall;
var
    joRes: Variant;
begin
     //生成返回值数组
    joRes := _Json('[]');
    {
    //以下是TChart时的代码,供参考
    joRes.Add('<script src="dist/charts/echarts.min.js"></script>');
    joRes.Add('<script src="dist/charts/lib/index.min.js"></script>');
    joRes.Add('<link rel="stylesheet" href="dist/charts/lib/style.min.css">');
    }
    joRes.Add('<script src="dist/ex/dwStringGrid.js"></script>');
    //
    Result := joRes;
end;

//根据JSON对象AData执行当前控件的事件, 并返回结果字符串
function dwGetEvent(ACtrl: TComponent; AData: string): string; stdcall;
var
    joData: Variant;
begin
    joData := _Json(AData);

    TDBGrid(ACtrl).DataSource.DataSet.RecNo := joData.v + 1;

    //执行事件
    if Assigned(TDBGrid(ACtrl).OnCellClick) then
    begin
        TDBGrid(ACtrl).OnCellClick(TDBGrid(ACtrl).Columns[0]);
    end;

end;

//取得HTML头部消息
function dwGetHead(ACtrl: TComponent): string; stdcall;
var
    iItem: Integer;
    joHint: Variant;
    joRes: Variant;
begin
    //生成返回值数组
    joRes := _Json('[]');

    //取得HINT对象JSON
    joHint := dwGetHintJson(TControl(ACtrl));

    with TDBGrid(ACtrl) do begin
        //添加外框
        joRes.Add('<div'
                + dwLTWH(TControl(ACtrl)) + '"' //style 封闭
                + '>');
        //添加主体
        joRes.Add('    <el-table'
                +' id="'+dwFullName(Actrl)+'"'
                + ' :data="' + dwFullName(Actrl) + '__ces"'
                + ' highlight-current-row'
                + ' ref="' + dwFullName(Actrl) + '"'                    //+' stripe'
                + dwIIF(Borderstyle <> bsNone, ' border', '')
                + dwVisible(TControl(ACtrl))
                + dwDisable(TControl(ACtrl))
                + ' height="' + IntToStr(TControl(ACtrl).Height)+ '"'
                + ' :row-style="{height:'''+dwIIF(HelpContext<=0,'26',IntToStr(HelpContext))+'px''}"' //行高
                + ' :header-row-style="{height:'''+dwIIF(HelpContext<=0,'26',IntToStr(HelpContext))+'px''}"' //行高
                + dwGetDWAttr(joHint)
                //
                + ' style="width:100%;'
                + dwGetDWStyle(joHint)
                +'"'
                + Format(_DWEVENT, ['row-click', Name, 'val.d0', 'onchange', TForm(Owner).Handle])
                + '>');
        //添加另外加的行号列, 用于表示行号
        joRes.Add('        <el-table-column  show-overflow-tooltip fixed v-if=false prop="d0" label="rowno" width="80"></el-table-column>');
        //添加各列
        for iItem := 0 to Columns.Count - 1 do begin
            joRes.Add('        <el-table-column' + ' show-overflow-tooltip' + ' prop="d' + IntToStr(iItem + 1) + '"' + ' label="' + Columns[iItem].Title.Caption + '"' + ' width="' + IntToStr(Columns[iItem].Width) + '"></el-table-column>');
        end;
    end;

    //log.WriteLog('取得HTML头部消息：'+joRes);
    Result := (joRes);
end;

//取得HTML尾部消息
function dwGetTail(ACtrl: TComponent): string; stdcall;
var
    joRes: Variant;
begin
    //生成返回值数组
    joRes := _Json('[]');

    //生成返回值数组
    joRes.Add('    </el-table>');
    joRes.Add('</div>');

    Result := (joRes);
end;


//取得Data
function dwGetData(ACtrl: TControl): string; stdcall;
var
    joRes       : Variant;
    iRow, iCol  : Integer;
    sCode       : string;
    oDataSet    : TDataSet;
    oBookMark   : TBookMark;
    oAfter      : Procedure(DataSet: TDataSet) of Object;
    oBefore     : Procedure(DataSet: TDataSet) of Object;
begin
    //生成返回值数组
    joRes := _Json('[]');
    with TDBGrid(ACtrl) do begin
        //取得数据集，备用
        oDataSet    := nil;
        if (DataSource <> nil) and (DataSource.DataSet <> nil ) then begin
            oDataSet := DataSource.DataSet;
        end;
        //添加基本数据
        joRes.Add(dwFullName(Actrl) + '__lef:"' + IntToStr(Left) + 'px",');
        joRes.Add(dwFullName(Actrl) + '__top:"' + IntToStr(Top) + 'px",');
        joRes.Add(dwFullName(Actrl) + '__wid:"' + IntToStr(Width) + 'px",');
        joRes.Add(dwFullName(Actrl) + '__hei:"' + IntToStr(Height) + 'px",');
        joRes.Add(dwFullName(Actrl) + '__vis:' + dwIIF(Visible, 'true,', 'false,'));
        joRes.Add(dwFullName(Actrl) + '__dis:' + dwIIF(Enabled, 'false,', 'true,'));

        //取得数据集
        if oDataSet <> nil then begin
            if not oDataSet.Active then begin
                sCode := dwFullName(Actrl) + '__ces:[],';
                joRes.Add(sCode);
            end else begin
                //保存当前位置
                oBookMark := oDataSet.GetBookmark;

                oDataSet.DisableControls;

                //保存原事件函数
                oAfter  := oDataSet.AfterScroll;
                oBefore := oDataSet.BeforeScroll;
                //清空事件
                oDataSet.AfterScroll    := nil;
                oDataSet.BeforeScroll   := nil;

                sCode := '';
                oDataSet.First;
                iRow := 0;
                while not oDataSet.Eof do begin
                    if sCode = '' then begin
                        sCode := dwFullName(Actrl) + '__ces:[{"d0":''' + IntToStr(iRow) + ''',';
                    end else begin
                        sCode := '{"d0":''' + IntToStr(iRow) + ''',';
                    end;
                    for iCol := 0 to Columns.Count - 1 do begin
                        sCode := sCode + '"d' + IntToStr(iCol + 1) + '":''' + _GetValue(Columns[iCol].Field) + ''',';
                    end;
                    //sCode := sCode + '"d' + IntToStr(Columns.Count - 1) + '":''' + _GetValue(Columns[Columns.Count - 1].Field) + '''}';
                    sCode := DeleteLastStr(sCode) + '}';
                    oDataSet.Next;
                    Inc(iRow);
                    if oDataSet.Eof then begin
                        joRes.Add(sCode + '],');
                    end else begin
                        joRes.Add(sCode + ',');
                    end;
                end;

                oDataSet.GotoBookmark(oBookMark); //重新定位记录指针回到原来的位置
                oDataSet.EnableControls;

                oDataSet.FreeBookmark(oBookMark); //删除书签BookMark标志
                //恢复原事件函数
                oDataSet.AfterScroll    := oAfter  ;
                oDataSet.BeforeScroll   := oBefore ;
            end;
        end else begin
            sCode := dwFullName(Actrl) + '__ces:[],';
            joRes.Add(sCode);
        end
    end;
    //log.WriteLog('取得Data：'+joRes);
    Result := (joRes);
end;

//取得Method
function dwGetAction(ACtrl: TControl): string; stdcall;
var
    joRes: Variant;
    iRow, iCol: Integer;
    sCode: string;
    oDataSet: TDataSet;
    oBookMark: TBookMark;
    oAfter  : Procedure(DataSet: TDataSet) of Object;
    oBefore : Procedure(DataSet: TDataSet) of Object;
begin
    //生成返回值数组
    joRes := _Json('[]');

    with TDBGrid(ACtrl) do begin
        //取得数据集，备用
        oDataSet    := nil;
        if (DataSource <> nil) and (DataSource.DataSet <> nil ) then begin
            oDataSet := DataSource.DataSet;
        end;

        joRes.Add('this.'+dwFullName(Actrl) + '__lef="' + IntToStr(Left) + 'px";');
        joRes.Add('this.'+dwFullName(Actrl) + '__top="' + IntToStr(Top) + 'px";');
        joRes.Add('this.'+dwFullName(Actrl) + '__wid="' + IntToStr(Width) + 'px";');
        joRes.Add('this.'+dwFullName(Actrl) + '__hei="' + IntToStr(Height) + 'px";');
        joRes.Add('this.'+dwFullName(Actrl) + '__vis=' + dwIIF(Visible, 'true;', 'false;'));
        joRes.Add('this.'+dwFullName(Actrl) + '__dis=' + dwIIF(Enabled, 'false;', 'true;'));

        if oDataSet <> nil then begin
            if not oDataSet.Active then begin
                sCode := 'this.'+ dwFullName(Actrl) + '__ces=[];';
                joRes.Add(sCode);
            end else begin
                //保存当前位置
                oBookMark := oDataSet.GetBookmark;
                oDataSet.DisableControls;

                //保存原事件函数
                oAfter  := oDataSet.AfterScroll;
                oBefore := oDataSet.BeforeScroll;
                //清空事件
                oDataSet.AfterScroll    := nil;
                oDataSet.BeforeScroll   := nil;
                //
                oDataSet.First;
                iRow := 0;
                sCode     := 'this.'+dwFullName(Actrl)+'__ces=[';
                while not oDataSet.Eof do begin
                    sCode     := sCode + '{d0:'''+IntToStr(iRow)+''',';
                    for iCol := 0 to Columns.Count - 1 do begin
                        sCode := sCode + 'd' + IntToStr(iCol + 1) + ':"' + _GetValue(Columns[iCol].Field) + '",';
                    end;
                    Delete(sCode,Length(sCode),1);
                    sCode     := sCode + '},';
                    //sCode := sCode + 'd' + IntToStr(Columns.Count - 1) + ':"' + _GetValue(Columns[Columns.Count - 1].Field) + '"});';
                    oDataSet.Next;
                    Inc(iRow);
                end;
                //如果有数据，则删除最后的逗号
                if oDataSet.RecordCount>0 then begin
                    Delete(sCode,Length(sCode),1);
                end;
                //
                sCode := sCode + '];';
                joRes.Add(sCode);
                //返回原位置
                oDataSet.GotoBookmark(oBookMark); //重新定位记录指针回到原来的位置
                oDataSet.EnableControls;
                //
                oDataSet.FreeBookmark(oBookMark); //删除书签BookMark标志
                //恢复原事件函数
                oDataSet.AfterScroll    := oAfter  ;
                oDataSet.BeforeScroll   := oBefore ;

(*
            //内容 cells
            sCode     := 'this.'+dwFullName(Actrl)+'__ces=[';
            for iRow := 1 to RowCount-1 do begin
                 sCode     := sCode + '{d0:'''+IntToStr(iRow)+''',';
                 for iCol := 0 to ColCount-1 do begin
                      sCode     := sCode + 'd'+IntToStr(iCol+1)+':'''+_ProcessCell(Cells[iCol,iRow])+''',';
                 end;
                 Delete(sCode,Length(sCode),1);
                 sCode     := sCode + '},';
            end;
            if RowCount>1 then begin
                 Delete(sCode,Length(sCode),1);
            end;
            sCode     := sCode + '];';
            joRes.Add(sCode);

*)
                //行号        this.$refs.multiplePlan.data[0]
                joRes.Add('this.$refs.' + TDBGrid(ACtrl).Name + '.setCurrentRow('
                        + 'this.$refs.' + TDBGrid(ACtrl).Name + '.data['
                        + IntToStr(TDBGrid(ACtrl).DataSource.DataSet.RecNo - 1) + ']' + ');');

            end;
        end else begin
            sCode := 'this.'+dwFullName(Actrl) + '__ces=[];';
            joRes.Add(sCode);
        end;

    end;
    //log.WriteLog('取得Method：'+joRes);
    Result := joRes;
end;


exports
    dwGetExtra,
    dwGetEvent,
    dwGetHead,
    dwGetTail,
    dwGetAction,
    dwGetData;

begin
end.

