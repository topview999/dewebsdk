﻿library dwTTrackBar__wwpage;

uses
     ShareMem,      //必须添加

     //
     dwCtrlBase,    //一些基础函数

     //
     SynCommons,    //mormot用于解析JSON的单元

     //
     Math,
     ComCtrls,
     SysUtils,
     Classes,
     Dialogs,
     StdCtrls,
     Windows,
     Controls,
     Forms;

//当前控件需要引入的第三方JS/CSS ,一般为不做改动,目前仅在TTrackBar使用时需要用到
function dwGetExtra(ACtrl:TComponent):string;stdCall;
var
     joRes     : Variant;
begin
     //生成返回值数组
     joRes    := _Json('[]');

     //需要额外引的代码
     //joRes.Add('<script src="dist/_vcharts/echarts.min.js"></script>');
     //joRes.Add('<script src="dist/_vcharts/lib/index.min.js"></script>');
     //joRes.Add('<link rel="stylesheet" href="dist/_vcharts/lib/style.min.css">');


     //
     Result    := joRes;
end;

//根据JSON对象AData执行当前控件的事件, 并返回结果字符串
function dwGetEvent(ACtrl:TComponent;AData:String):string;StdCall;
var
    joData  : Variant;
    iTmp    : Integer;
    oChange : Procedure(Sender:TObject) of Object;
begin
    with TTrackBar(Actrl) do begin
        //用作分页控件--------------------------------------------------------------------------

        //
        joData    := _Json(AData);


        if joData.e = 'onchange' then begin
            //保存事件
            oChange   := TTrackBar(ACtrl).OnChange;
            //清空事件,以防止自动执行
            TTrackBar(ACtrl).OnChange  := nil;
            //更新值
            iTmp    := StrToIntDef(dwUnescape(joData.v),0);
            iTmp    := Math.Max(iTmp,1);
            iTmp    := Math.Min(iTmp,Ceil(Max/PageSize));
            TTrackBar(ACtrl).Position    := iTmp;//StrToIntDef(dwUnescape(joData.v),0);
            //恢复事件
            TTrackBar(ACtrl).OnChange  := oChange;

            //执行事件
            if Assigned(TTrackBar(ACtrl).OnChange) then begin
                TTrackBar(ACtrl).OnChange(TTrackBar(ACtrl));
            end;
        end;
    end;
end;


//取得HTML头部消息
function dwGetHead(ACtrl:TComponent):string;StdCall;
var
    sCode       : string;
    joHint      : Variant;
    joRes       : Variant;
    sType       : string;
    iMax,iMin   : Integer;
    iTotal      : Integer;
    iLeft       : Integer;
begin
    with TTrackBar(Actrl) do begin
        //用作分页控件--------------------------------------------------------------------------

        //生成返回值数组
        joRes    := _Json('[]');

        //取得HINT对象JSON
        joHint    := dwGetHintJson(TControl(ACtrl));

        with TTrackBar(ACtrl) do begin
            if Position = 0 then begin
                Position    := 1;
            end;
            //外框
            sCode     := '<div'
                    +' :style="{'
                            +'left:'+dwFullName(Actrl)+'__lef,'
                            +'top:'+dwFullName(Actrl)+'__top,'
                            +'width:'+dwFullName(Actrl)+'__wid,'
                            +'height:'+dwFullName(Actrl)+'__hei}"'
                    +' style="position:absolute;color:#969696;'
                    +dwGetDWStyle(joHint)
                    +'"' //style 封闭
                    +'>';
            //添加到返回值数据
            joRes.Add(sCode);

            //prev按钮
            sCode     := '    <el-button v-for="(item,i) in '+dwFullName(ACtrl)+'__lst" '
                    //+' id="'+dwFullName(Actrl)+'__pr"'
                    +' style="position:absolute;top:10px;width:30px;height:30px;border:0;"'
                    +' :icon="item.icon"' //el-icon-arrow-left"'
                    //+' type="primary"'
                    +' :key="i"'
                    //+' :plain="item.plain"'

                    +' :style="{left:item.left,color:item.color,''background-color'':item.bkcolor}"'
                    +' @click=dwevent("","'+dwFullName(Actrl)+'",item.dest,"onchange",'+IntToStr(TForm(Owner).Handle)+');'
                    +'>'
                    +'{{item.caption}}'
                    +'</el-button>';
            joRes.Add(sCode);

            //求当前按钮的最小值和最大值
            iTotal  := Ceil(Max/PageSize);
            if Position <= 3 then begin
                iMin    := 1;
                iMax    := Math.Min(iTotal,iMin+4);
            end else if Position >= iTotal-2 then begin
                iMax    := iTotal;
                iMin    := Math.Max(1,iTotal-4);
            end else begin
                iMin    := Position - 2;
                iMax    := iMin + 4;
            end;

            //<---跳转
            iLeft   := 10+40+(iMax-iMin+1)*40+40;
            if Width > iLeft + 300 then begin
                //
                sCode     := '    <div'
                        //+' id="'+dwFullName(Actrl)+'__pr"'
                        +' style="position:absolute;left:'+IntToStr(iLeft)+'px;top:10px;width:50px;line-height:30px;height:30px;text-align:right"'
                        +'>前往'
                        +'</div>';
                //添加到返回值数据
                joRes.Add(sCode);
                iLeft   := iLeft + 50;

                //next按钮
                sCode     := '    <el-input'
                        +' v-model='+dwFullName(Actrl)+'__cpg'
                        +' style="position:absolute;left:'+IntToStr(iLeft)+'px;top:10px;width:50px;line-height:30px;height:30px;text-align:center"'
                        +' @change=function(val){dwevent("","'+dwFullName(Actrl)+'",val,"onchange",'+IntToStr(TForm(Owner).Handle)+');}'
                        +'>'
                        +'</el-input>';
                //添加到返回值数据
                joRes.Add(sCode);
                iLeft   := iLeft + 50;

                //next按钮
                sCode     := '    <div'
                        +' style="position:absolute;left:'+IntToStr(iLeft)+'px;top:10px;width:50px;line-height:30px;height:30px;"'
                        +'>页'
                        +'</div>';
                //添加到返回值数据
                joRes.Add(sCode);
                iLeft   := iLeft + 50;
            end;
            //>----

            //<---总页数
            if  width >= (10+40+(iMax-iMin+1)*40+40)+150 then begin
                iLeft   := Width-150;
                //
                sCode     := '    <div'
                        //+' id="'+dwFullName(Actrl)+'__pr"'
                        +' style="position:absolute;left:'+IntToStr(iLeft)+'px;top:10px;width:50px;line-height:30px;height:30px;text-align:right"'
                        +'>总'
                        +'</div>';
                //添加到返回值数据
                joRes.Add(sCode);
                iLeft   := iLeft + 50;

                //next按钮
                sCode     := '    <div'
                        +' value="1"'
                        +' style="position:absolute;left:'+IntToStr(iLeft)+'px;top:10px;width:50px;line-height:30px;height:30px;text-align:center"'
                        +'>{{'+dwFullName(Actrl)+'__tot}}'
                        +'</div>';
                //添加到返回值数据
                joRes.Add(sCode);
                iLeft   := iLeft + 50;

                //next按钮
                sCode     := '    <div'
                        +' style="position:absolute;left:'+IntToStr(iLeft)+'px;top:10px;width:50px;line-height:30px;height:30px;"'
                        +'>条'
                        +'</div>';
                //添加到返回值数据
                joRes.Add(sCode);
                iLeft   := iLeft + 50;
            end;
            //>----
        end;
        //
        Result    := (joRes);
    end;
end;

//取得HTML尾部消息
function dwGetTail(ACtrl:TComponent):string;StdCall;
var
     joRes     : Variant;
     sType     : string;
begin
    with TTrackBar(Actrl) do begin
        //用作分页控件--------------------------------------------------------------------------

        //生成返回值数组
        joRes    := _Json('[]');

        //生成返回值数组
        //joRes.Add('    </el-pagination>');               //此处需要和dwGetHead对应
        joRes.Add('</div>');               //此处需要和dwGetHead对应
        //
        Result    := (joRes);
    end;
end;

//取得Data
function dwGetData(ACtrl:TComponent):string;StdCall;
var
    iSeries     : Integer;
    iItem       : Integer;
    iMax,iMin   : Integer;
    iLeft       : Integer;
    iTotal      : Integer;
    //
    joRes       : Variant;
    //
    sCode       : String;
    sGrid       : String;
    joList      : Variant;
    joItem      : Variant;
begin
    with TTrackBar(Actrl) do begin
        //用作分页控件--------------------------------------------------------------------------

        //生成返回值数组
        joRes    := _Json('[]');
        //
        with TTrackBar(ACtrl) do begin
            //基本数据
            joRes.Add(dwFullName(Actrl)+'__lef:"'+IntToStr(Left)+'px",');
            joRes.Add(dwFullName(Actrl)+'__top:"'+IntToStr(Top)+'px",');
            joRes.Add(dwFullName(Actrl)+'__wid:"'+IntToStr(Width)+'px",');
            joRes.Add(dwFullName(Actrl)+'__hei:"'+IntToStr(Height)+'px",');
            //
            joRes.Add(dwFullName(Actrl)+'__vis:'+dwIIF(Visible,'true,','false,'));
            joRes.Add(dwFullName(Actrl)+'__dis:'+dwIIF(not Enabled,'true,','false,'));


            //
            //joRes.Add(dwFullName(Actrl)+':'+IntToStr(Position)+',');
            //
            joRes.Add(dwFullName(Actrl)+'__pgs:'+IntToStr(Math.Max(1,PageSize))+',');
            joRes.Add(dwFullName(Actrl)+'__cpg:'+IntToStr(Math.Max(1,Position))+',');
            joRes.Add(dwFullName(Actrl)+'__tot:'+IntToStr(Max)+',');

            //求当前按钮的最小值和最大值
            iTotal  := Ceil(Max/PageSize);
            if Position <= 3 then begin
                iMin    := 1;
                iMax    := Math.Min(iTotal,iMin+4);
            end else if Position >= iTotal-2 then begin
                iMax    := iTotal;
                iMin    := Math.Max(1,iTotal-4);
            end else begin
                iMin    := Position - 2;
                iMax    := iMin + 4;
            end;

            joList  := _json('[]');
            //前一页
            iLeft           := 10;
            joItem          := _json('{}');
            joItem.left     := IntToStr(iLeft)+'px';    //left
            joItem.icon     := 'el-icon-arrow-left';    //icon
            joItem.caption  := '';
            joItem.dest     := Math.Max(1,Position-1);
            joItem.color    := '#303030';
            joItem.bkcolor  := '#f4f4f4';
            joList.Add(joItem);
            iLeft           := iLeft+40;
            for iItem := iMin to iMax do begin
                joItem          := _json('{}');
                joItem.left     := IntToStr(iLeft)+'px';
                joItem.icon     := '';
                joItem.caption  := IntToStr(iItem);
                joItem.dest     := iItem;
                if iItem = Position then begin
                    joItem.bkcolor  := '#409eff';
                    joItem.color    := '#ffffff';
                end else begin
                    joItem.color    := '#969696';
                    joItem.bkcolor  := '#f4f4f4';
                end;
                joList.Add(joItem);
                iLeft           := iLeft+40;
            end;
            //后一页
            joItem          := _json('{}');
            joItem.left     := IntToStr(iLeft)+'px';
            joItem.icon     := 'el-icon-arrow-right';
            joItem.caption  := '';
            joItem.dest     := Math.Min(iTotal,Position+1);
            joItem.color    := '#303030';
            joItem.bkcolor  := '#f4f4f4';
            joList.Add(joItem);
            //
            sCode   := VariantSaveJSON(joList);
            joRes.Add(dwFullName(Actrl)+'__lst:'+sCode+',');

        end;
        //
        Result    := (joRes);
    end;
end;

function dwGetAction(ACtrl:TComponent):string;StdCall;
var
    iSeries     : Integer;
    iItem       : Integer;
    iMax,iMin   : Integer;
    iLeft       : Integer;
    iTotal      : Integer;
    //
    joRes       : Variant;
    //
    sCode       : String;
    sGrid       : String;
    joList      : Variant;
    joItem      : Variant;
begin
    with TTrackBar(Actrl) do begin
        //用作分页控件--------------------------------------------------------------------------

        //生成返回值数组
        joRes    := _Json('[]');
        //
        with TTrackBar(ACtrl) do begin
            //基本数据
            joRes.Add('this.'+dwFullName(Actrl)+'__lef="'+IntToStr(Left)+'px";');
            joRes.Add('this.'+dwFullName(Actrl)+'__top="'+IntToStr(Top)+'px";');
            joRes.Add('this.'+dwFullName(Actrl)+'__wid="'+IntToStr(Width)+'px";');
            joRes.Add('this.'+dwFullName(Actrl)+'__hei="'+IntToStr(Height)+'px";');
            //
            joRes.Add('this.'+dwFullName(Actrl)+'__vis='+dwIIF(Visible,'true;','false;'));
            joRes.Add('this.'+dwFullName(Actrl)+'__dis='+dwIIF(not Enabled,'true;','false;'));


            //
            //joRes.Add(dwFullName(Actrl)+':'+IntToStr(Position)+',');
            //
            joRes.Add('this.'+dwFullName(Actrl)+'__pgs='+IntToStr(Math.Max(1,PageSize))+';');
            joRes.Add('this.'+dwFullName(Actrl)+'__cpg='+IntToStr(Math.Max(1,Position))+';');
            joRes.Add('this.'+dwFullName(Actrl)+'__tot='+IntToStr(Max)+';');

            //求当前按钮的最小值和最大值
            iTotal  := Ceil(Max/PageSize);
            if Position <= 3 then begin
                iMin    := 1;
                iMax    := Math.Min(iTotal,iMin+4);
            end else if Position >= iTotal-2 then begin
                iMax    := iTotal;
                iMin    := Math.Max(1,iTotal-4);
            end else begin
                iMin    := Position - 2;
                iMax    := iMin + 4;
            end;

            joList  := _json('[]');
            //前一页
            iLeft           := 10;
            joItem          := _json('{}');
            joItem.left     := IntToStr(iLeft)+'px';    //left
            joItem.icon     := 'el-icon-arrow-left';    //icon
            joItem.caption  := '';
            joItem.dest     := Math.Max(1,Position-1);
            joItem.color    := '#303030';
            joItem.bkcolor  := '#f4f4f4';
            joList.Add(joItem);
            iLeft           := iLeft+40;
            for iItem := iMin to iMax do begin
                joItem          := _json('{}');
                joItem.left     := IntToStr(iLeft)+'px';
                joItem.icon     := '';
                joItem.caption  := IntToStr(iItem);
                joItem.dest     := iItem;
                if iItem = Position then begin
                    joItem.bkcolor  := '#409eff';
                    joItem.color    := '#ffffff';
                end else begin
                    joItem.color    := '#969696';
                    joItem.bkcolor  := '#f4f4f4';
                end;
                joList.Add(joItem);
                iLeft           := iLeft+40;
            end;
            //后一页
            joItem          := _json('{}');
            joItem.left     := IntToStr(iLeft)+'px';
            joItem.icon     := 'el-icon-arrow-right';
            joItem.caption  := '';
            joItem.dest     := Math.Min(iTotal,Position+1);
            joItem.color    := '#303030';
            joItem.bkcolor  := '#f4f4f4';
            joList.Add(joItem);
            //
            sCode   := VariantSaveJSON(joList);
            joRes.Add('this.'+dwFullName(Actrl)+'__lst='+sCode+';');

        end;
        //
        Result    := (joRes);
    end;
end;


exports
     //dwGetExtra,
     dwGetEvent,
     dwGetHead,
     dwGetTail,
     dwGetAction,
     dwGetData;
     
begin
end.
 
