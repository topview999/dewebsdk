﻿library dwTBitBtn;

uses
     ShareMem,

     //
     dwCtrlBase,

     //
     SynCommons,

     //
     Buttons,
     SysUtils,
     Classes,
     Dialogs,
     StdCtrls,
     Windows,
     Controls,
     Forms;

//当前控件需要引入的第三方JS/CSS
function dwGetExtra(ACtrl:TComponent):String;stdCall;
begin
     Result    := '[]';
end;

//根据JSON对象AData执行当前控件的事件, 并返回结果字符串
function dwGetEvent(ACtrl:TComponent;AData:String):String;StdCall;
var
    joData  : Variant;
    oObject : TDragDockObject;
begin
     //
     joData    := _Json(AData);

     if joData.e = 'onenddock' then begin
          if Assigned(TBitBtn(ACtrl).OnEndDock) then begin
               TBitBtn(ACtrl).OnEndDock(TBitBtn(ACtrl),nil,0,0);
          end;
     end else if joData.e = 'onstartdock' then begin
          if Assigned(TBitBtn(ACtrl).OnStartDock) then begin
               TBitBtn(ACtrl).OnStartDock(TBitBtn(ACtrl),oObject);
          end;
     end else if joData.e = 'onenter' then begin
          if Assigned(TBitBtn(ACtrl).OnEnter) then begin
               TBitBtn(ACtrl).OnEnter(TBitBtn(ACtrl));
          end;
     end else if joData.e = 'onexit' then begin
          if Assigned(TBitBtn(ACtrl).OnExit) then begin
               TBitBtn(ACtrl).OnExit(TBitBtn(ACtrl));
          end;
     end else if joData.e = 'getfilename' then begin
          //TBitBtn(ACtrl).Caption   := dwUnescape(joData.v);
     end;
end;


//取得HTML头部消息
function dwGetHead(ACtrl:TComponent):String;StdCall;
var
    sCode   : String;
    sSize   : String;
    sDir    : String;   //自定义上传目录
    //
    bAuto   : Boolean;

    //
    joHint  : Variant;
    joRes   : Variant;
    iHandle : Integer;
begin
    //生成返回值数组
    joRes    := _Json('[]');

    //取得HINT对象JSON
    joHint    := dwGetHintJson(TControl(ACtrl));

    //_DWEVENT = ' @%s="dwevent($event,''%s'',''%s'',''%s'',''%s'')"';
    //参数依次为: JS事件名称, 控件名称,控件值,Delphi事件名称,备用


    //
    with TBitBtn(ACtrl) do begin
        //添加Form
        joRes.Add('<form'
                +' id="'+dwFullName(Actrl)+'__frm"'
                +dwVisible(TControl(ACtrl))
                +dwDisable(TControl(ACtrl))
                +dwLTWH(TControl(ACtrl))
                +'"' //style 封闭
                +' action="/dwupload"'
                +' method="POST"'
                +' enctype="multipart/form-data"'
                +' target="upload_iframe"'
                +'>');

        //在Hint中"auto":1表示自动上传
        bAuto   := False;
        if joHint.Exists('auto') then begin
            bAuto  := Integer(joHint.auto) = 1;
        end;

        //取上传目录. 格式: mydir\dasdf  前面没有\, 后面无\
        sDir    := '';
        if joHint.Exists('dir') then begin
            sDir  := String(joHint.dir);
        end;
        sDir    := Trim(sDir);
        if sDir <> '' then begin
            //去除前面的\
            if sDir[1] = '\' then begin
                Delete(sDir,1,1);
            end;
            //去除后面的\
            if sDir[Length(sDir)]='\' then begin
                Delete(sDir,Length(sDir),1);
            end;
        end;
        //把中间的\转化为\\\\,以适应JS的转义需求
        //sDir    := StringReplace(sDir,'\','\\\\',[rfReplaceAll]);



        //求Form的Handle
        //
        iHandle := TForm(Actrl.Owner).Handle;
        if lowerCase(ACtrl.Owner.ClassName) <> 'tform1' then begin
            //iHandle := TForm(TForm(Actrl.Owner).Owner).Handle;
        end;


        //添加Input
		joRes.Add('<input id="'+dwFullName(Actrl)+'__inp" type="FILE" name="file"'
                  +' style="display:none"'
                  +dwGetHintValue(joHint,'accept','accept','')
                  +dwGetHintValue(joHint,'capture','capture','')
                  +dwIIF(bAuto,' @change=dwInputSubmit('+iHandle.ToString+','''+dwFullName(Actrl)+''','''+sDir+''');',
                          ' @change=dwInputChange('+iHandle.ToString+','''+dwFullName(Actrl)+''');')
                  +'>');


        //添加Button
        //得到大小：large/medium/small/mini
        if Height>50 then begin
             sSize     := ' size=large';
        end else if Height>35 then begin
             sSize     := ' size=medium';
        end else if Height>20 then begin
             sSize     := ' size=small';
        end else begin
             sSize     := ' size=mini';
        end;

        //
        sCode     := '<el-button'
                +' id="'+dwFullName(Actrl)+'__btn"'
                +sSize
                +dwVisible(TControl(ACtrl))
                +dwDisable(TControl(ACtrl))
                //+dwGetHintValue(joHint,'type','type',' type="default"')         //sButtonType
                +' :type="'+dwFullName(Actrl)+'__typ"'
                +dwGetHintValue(joHint,'icon','icon','')         //ButtonIcon
                +dwGetHintValue(joHint,'style','','')             //样式，空（默认）/plain/round/circle
                //
                +dwGetDWAttr(joHint)
                //
                +' style="position:absolute;width:100%;height:100%;'
                +dwGetHintStyle(joHint,'radius','border-radius','')   //border-radius
                +dwGetDWStyle(joHint)
                +'"'
                //默认选择文件
                +' @click="dwInputClick('''+dwFullName(Actrl)+'__inp'');"'

                //其他事件
                //+dwIIF(Assigned(OnClick),Format(_DWEVENT,['click',Name,'0','onclick',TForm(Owner).Handle]),'')
                +dwIIF(Assigned(OnEnter),Format(_DWEVENT,['mouseenter.native',dwFullName(Actrl),'0','onenter',TForm(Owner).Handle]),'')
                +dwIIF(Assigned(OnExit),Format(_DWEVENT,['mouseleave.native',dwFullName(Actrl),'0','onexit',TForm(Owner).Handle]),'')
                +'>{{'+dwFullName(Actrl)+'__cap}}';
        //
        joRes.Add(sCode);

    end;

    Result    := (joRes);
    //
    //@mouseenter.native=“enter”
end;

//取得HTML尾部消息
function dwGetTail(ACtrl:TComponent):String;StdCall;
var
     joRes     : Variant;
begin
     //生成返回值数组
     joRes    := _Json('[]');
     //生成返回值数组
     joRes.Add('</el-button>');
     joRes.Add('</form>');
     //
     Result    := (joRes);
end;

//取得Data消息
function dwGetData(ACtrl:TComponent):String;StdCall;
var
     joRes     : Variant;
begin
     //生成返回值数组
     joRes    := _Json('[]');
     //
     with TBitBtn(ACtrl) do begin
          joRes.Add(dwFullName(Actrl)+'__lef:"'+IntToStr(Left)+'px",');
          joRes.Add(dwFullName(Actrl)+'__top:"'+IntToStr(Top)+'px",');
          joRes.Add(dwFullName(Actrl)+'__wid:"'+IntToStr(Width)+'px",');
          joRes.Add(dwFullName(Actrl)+'__hei:"'+IntToStr(Height)+'px",');
          //
          joRes.Add(dwFullName(Actrl)+'__vis:'+dwIIF(Visible,'true,','false,'));
          joRes.Add(dwFullName(Actrl)+'__dis:'+dwIIF(Enabled,'false,','true,'));
          //
          joRes.Add(dwFullName(Actrl)+'__cap:"'+dwProcessCaption(Caption)+'",');
          //
          joRes.Add(dwFullName(Actrl)+'__typ:"'+dwGetProp(TBitBtn(ACtrl),'type')+'",');
     end;
     //
     Result    := (joRes);
end;

//取得事件
function dwGetAction(ACtrl:TComponent):String;StdCall;
var
     joRes     : Variant;
begin
     //生成返回值数组
     joRes    := _Json('[]');
     //
     with TBitBtn(ACtrl) do begin
          joRes.Add('this.'+dwFullName(Actrl)+'__lef="'+IntToStr(Left)+'px";');
          joRes.Add('this.'+dwFullName(Actrl)+'__top="'+IntToStr(Top)+'px";');
          joRes.Add('this.'+dwFullName(Actrl)+'__wid="'+IntToStr(Width)+'px";');
          joRes.Add('this.'+dwFullName(Actrl)+'__hei="'+IntToStr(Height)+'px";');
          //
          joRes.Add('this.'+dwFullName(Actrl)+'__vis='+dwIIF(Visible,'true;','false;'));
          joRes.Add('this.'+dwFullName(Actrl)+'__dis='+dwIIF(Enabled,'false;','true;'));
          //
          joRes.Add('this.'+dwFullName(Actrl)+'__cap="'+dwProcessCaption(Caption)+'";');
          //
          joRes.Add('this.'+dwFullName(Actrl)+'__typ="'+dwGetProp(TBitBtn(ACtrl),'type')+'";');
     end;
     //
     Result    := (joRes);
end;

exports
     //dwGetExtra,
     dwGetEvent,
     dwGetHead,
     dwGetTail,
     dwGetAction,
     dwGetData;

begin
end.

