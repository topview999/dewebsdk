# DeWebSDK

#### 介绍
DeWeb:Delphi开发者的Web解决方案！
DeWeb是一个可以直接将Delphi程序快速转换为网页应用的工具！
使用DeWeb, 开发者不需要学习HTML、JavaScript、Java、PHP、ASP、C#等新知识，用Delphi搞定一切。 
DeWeb开发的网页支持所有客户端，包括手机、平板等。

<br/>
首页<br/>
http://www.delphibbs.com
<br/>
仓库管理系统<br/>
http://www.delphibbs.com/dwms
大富翁论坛（试运行）<br/>
http://www.delphibbs.com/bbs
<br/>


本程序只提供DeWeb的开发包, 用于帮助感兴趣的朋友了解DeWeb的开发进度 

#### 使用说明

<br/>
DeWeb是一个可以直接将Delphi程序快速转换为网页应用的工具！

使用DeWeb, 开发者不需要学习HTML、JavaScript、Java、PHP、ASP、C#等新知识，
用Delphi搞定一切。 

DeWeb开发的网页支持所有客户端，包括手机、平板等。 

!!!目前支持采用D 10.2/10.3/10.4/11开发！！！

开发例程请参考demos中即可

#### 开发者

碧树西风
<br/>
-
<br/>
资深Delphi开发者
<br/>
QQ讨论群:120283369
<br/>
mail:fenglinyushu@163.com

